angular.module('app.directives', [])

.directive('googlePlaces', function() {
    var componentForm = {
        premise: 'long_name',
        street_number: 'short_name',
        route: 'long_name',
        sublocality_level_1: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
    };
    var mapping = {
        premise: 'BuildingName',
        street_number: 'Unit',
        route: 'Street',
        sublocality_level_1: 'Suburb',
        locality: 'City',
        administrative_area_level_1: 'State',
        country: 'Country',
        postal_code: 'PostCode'
        //Region, District, Level
    };
    // por defecto si no se ha elegido país se busca en direcciones en España
    var countryCode = ['es', 'ec', 'pa'];
    return {
        //require: 'ngModel',
        restrict: 'E',
        replace: true,
        scope: {
            address: '=?',
        },
        template: '<input id="google_places_ac" name="google_places_ac" type="text" class="input-block-level" />',
        link: function(scope, element, attrs, model) {
            var options = {
                componentRestrictions: { country: countryCode },
                types: ['geocode']
            };

            attrs.$observe('countryCode', function(value){
                console.log('cambio en countryCode: ' + value);
                autocomplete.setComponentRestrictions({'country': value});
            });

            //var autocomplete = new google.maps.places.Autocomplete($("#google_places_ac")[0], options);
            var autocomplete = new google.maps.places.Autocomplete(element[0], options);

            google.maps.event.addListener(autocomplete, 'place_changed', function() {
                var place = autocomplete.getPlace();

                var location = place.geometry && place.geometry.location ? {
                    Latitude: place.geometry.location.lat(),
                    Longitude: place.geometry.location.lng()
                } : {};

                // Get each component of the address from the place location
                // and fill the corresponding field on the form.
                for (var i = 0; i < place.address_components.length; i++) {
                    var addressType = place.address_components[i].types[0];
                    if (componentForm[addressType]) {
                        var val = place.address_components[i][componentForm[addressType]];
                        location[mapping[addressType]] = val;
                    }
                }
                location.FormattedAddress = place.formatted_address;
                location.PlaceId = place.place_id;

                // Valor que tendrá el input
                // TODO: limpiar si eso, porque esto ahora no se utiliza. Se deja como referencia
                if (location.Street) {
                    location.Value = location.Street;
                    if (location.City) {
                        location.Value += ', ' + location.City;
                    }
                } else {
                    location.Value = element[0].value;
                }


                scope.$apply(function () {
                    // Gracias al two-way binding definido en el isolated scope {} con 'address', se actualiza la
                    // variable en el scope padre
                    scope.address = location; // array containing each location component
                    //model.$setViewValue(location);
                });
            });
        }
    };
})

.directive('showHideContainer', function(){
    return {
        scope: {

        },
        controller: function($scope, $element, $attrs) {
            $scope.show = false;

            $scope.toggleType = function($event){
                $event.stopPropagation();
                $event.preventDefault();

                $scope.show = !$scope.show;

                // Emit event
                $scope.$broadcast("toggle-type", $scope.show);
            };
        },
        templateUrl: 'templates/misc/show-hide-password.html',
        restrict: 'A',
        replace: false,
        transclude: true
    };
})


.directive('showHideInput', function(){
    return {
        scope: {

        },
        link: function(scope, element, attrs) {
            // listen to event
            scope.$on("toggle-type", function(event, show){
                var password_input = element[0],
                    input_type = password_input.getAttribute('type');

                if(!show)
                {
                    password_input.setAttribute('type', 'password');
                }

                if(show)
                {
                    password_input.setAttribute('type', 'text');
                }
            });
        },
        require: '^showHideContainer',
        restrict: 'A',
        replace: false,
        transclude: false
    };
})

.directive('geoAddressesAlert', function($ionicPopup, GlobalsService){
    return {
        scope: true,
        controller: function($scope, $element, $attrs) {
            $scope.openAddressesModal = function () {
                if (!$scope.globalDoc.hideInitialMessage) {
                    $scope.updateAddressesPopup = $ionicPopup.show({
                        template: '<div style="text-align: center"> Estamos optimizando el reparto y a partir de ' +
                            'ahora todas las direcciones de entrega han de estar <b>geolocalizadas</b>. Actualiza ' +
                            'tus direcciones una sola vez y podremos ofrecerte el <b>mejor servicio</b> y tiempo de ' +
                            'entregamás rápido posible.</div>',
                        //templateUrl: 'templates/whatsapp-order-popup.html',
                        //cssClass: 'whatsapp-popup',
                        title: 'Actualiza tus direcciones',
                        //subTitle: 'Mejoramos nuestro servicio',
                        scope: $scope,
                        buttons: [
                             {
                                 text: 'Entendido, actualizar',
                                 type: 'button-calm',
                                 onTap: function () {
                                     // Se actualiza el flag para que no vuelva a mostrar el aviso de por qué
                                     // actualizar las direcciones
                                     $scope.globalDoc.hideInitialMessage = true;
                                     GlobalsService.updateGlobalDoc($scope.globalDoc);

                                     // Se abre la ventana modal
                                     $scope.updateAddressesModal.show();
                                 }
                             }
                        ]
                    });
                } else {
                    // Se abre la ventana modal
                    $scope.updateAddressesModal.show();
                }
            };
        },
        templateUrl: 'templates/update-old-addresses-alert.html',
        restrict: 'E'
    };
})