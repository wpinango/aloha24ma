angular.module('app.controllers', [])

.controller('AppCtrl', function($scope, $rootScope, $ionicLoading, $interval, apiConstants, User, Business,
                                GlobalsService, $cordovaDialogs, $state, $cordovaGeolocation, $ionicHistory, $ionicPopup,
                                $cordovaClipboard, $timeout, $ionicModal, AppMinimumVersionCheck, tmhDynamicLocale) {
    // Se muestra un 'loading' hasta que cargue la BD local
    if (!User.auth.dbLoaded) {
        $ionicLoading.show({
            template: 'Cargando...'
        });


        // Se escucha por el evento disparado al volver a primer plano la app
        document.addEventListener("resume", function() {
            window.FirebasePlugin.onNotificationOpen(function(notification) {
                console.log('onNotificationOpen por la del resume');
                console.log(JSON.stringify(notification));

                var body = '';
                var title = 'Notificación';

                if (device.platform.toLowerCase() == 'ios') {
                    console.log('PUSH ios');
                    try {
                        console.log(typeof notification.aps);
                        console.log(typeof notification.aps.alert);

                        if (notification.aps && notification.aps.alert) {
                            // En este caso sólo tenemos mensaje sin título
                            if (typeof notification.aps.alert === 'string') {
                                body = notification.aps.alert;
                            } else {
                                title = notification.aps.alert.title;
                                body = notification.aps.alert.body;
                            }
                        }
                    } catch (e) {
                        console.log('Error trying to reconstruct push ios: ' + e);
                    }
                } else if (device.platform.toLowerCase() == 'android') {
                    console.log('PUSH android');
                    if (notification.body) {
                        body = notification.body;
                    }
                    if (notification.title) {
                        title = notification.title;
                    }
                }

                if (body !== '') {
                    $cordovaDialogs.alert(body, title, 'OK');
                }
            }, function(error) {
                console.error(error);
            });

            // Se comprueba si la versión actual de la App corresponde a la última y si no se fuerza la actualización
            AppMinimumVersionCheck();
        }, false);
    }

    // Objeto wrapper para controlar si está logeado o no, que se controla/actualiza a través del servicio 'User'
    $scope.auth = User.auth;
    // Objeto wrapper para controlar el tema de si se ha de mostrar la selección del pais o el pais seleccionado
    $scope.countries = User.countries;
    $scope.globalDoc = {};
    $scope.selectedCountry = {};
    
    // Función para filtrar de las direcciones las que son antiguas y dejar el selector sólo con nuevas (geolocalizadas)
    var removeOldAddresses = function() {
        var i = $scope.geoAddresses.length;
        // Se recorren las direcciones actuales del usuario
        while (i--) {
            // Y se quitan las antiguas
            if ($scope.geoAddresses[i].address == '') {
                $scope.geoAddresses.splice(i, 1);
            }
        }
    };

    // Se espera a que la BD local se haya cargado
    $scope.$on('dbLoaded', function (event, globalDoc) {
        console.log('dbLoaded ON AppCtrl');
        // Se marca el flag
        User.auth.dbLoaded = true;

        // Se obtiene el documento de trabajo sobre la BD local
        $scope.globalDoc = globalDoc;
        $scope.countriesOptions = $scope.globalDoc.countries;
        
        // Se actualiza la versión de la API que hay que consumir con el valor guardado en local
        if (globalDoc.apiVersion) {
            Business.setApiVersion(globalDoc.apiVersion);
        }

        // Se comprueba la versión actual más reciente de la API en servidor
        Business.getVersion()
            .then(function (response) {

                var res = response.data.resultado;
                if (res.code == 0) {
                    $scope.globalDoc.apiVersion = res.vapi;
                    $scope.globalDoc.appVersion = res.vapp;
                    GlobalsService.updateGlobalDoc($scope.globalDoc);

                    //Se actualiza la lista de paises
                    var vCountries = GlobalsService.setCountries(res.businesses);
                    $scope.countriesOptions = vCountries;

                    // Se actualiza la versión de la API que hay que consumir
                    Business.setApiVersion(res.vapi);

                    // Se comprueba si la versión actual de la App corresponde a la última y si no se fuerza la actualización
                    AppMinimumVersionCheck();

                    // Se actualizan los datos de contacto de la empresa Aloha
                    Business.getContacto()
                        .then(function (response) {
                            var res = response.data.resultado;
                            if (res.code == 0) {
                                $scope.globalDoc.contacto = res.contacto;
                                GlobalsService.updateGlobalDoc($scope.globalDoc);
                            }
                        });
                }
            });

        // Si el usuario está logeado se realizan una serie de acciones
        if ($scope.globalDoc.user.token) {
            User.auth.isLogged = true;
            User.auth.token = $scope.globalDoc.user.token;
            Business.setAuthToken($scope.globalDoc.user.token);
            // Se actualizan los datos del usuario
            User.refreshData();

            // Si está logeado se muestra un selector con las direcciones de entrega habituales
            $scope.geoAddresses = angular.copy($scope.globalDoc.user.direcciones);
            removeOldAddresses();
        }

        // Si se ha seleccionado un país se actualiza
        if ($scope.globalDoc.countryIsSelected) {
            console.log('pais seleccionado');
            console.log($scope.globalDoc.selectedCountry);
            $scope.selectedCountry.country = $scope.globalDoc.selectedCountry;
            // Y se fija el idioma
            if ($scope.globalDoc.selectedCountry && $scope.globalDoc.selectedCountry.completeCode) {
                console.log('hago set del complete code');
                console.log($scope.globalDoc.selectedCountry.completeCode)
                tmhDynamicLocale.set($scope.globalDoc.selectedCountry.completeCode);
            }

            //$scope.globalDoc.countryIsSelected = false;
        }

        // Función que comprueba si el usuario tiene pendientes direcciones para actualizar (geolocalizar)
        $scope.hasOldAddresses = function () {
            return User.hasAddressesWithFormat('old');
        };
        // Se define la ventana modal para actualizar las direcciones antiguas
        $ionicModal.fromTemplateUrl('templates/update-addresses-modal.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function(modal) {
            $scope.updateAddressesModal = modal;
        });

        $ionicLoading.hide();

        $scope.$watch('wrapper.address', function() {
            console.log('watch: wrapper.address');
            if ($scope.globalDoc) {
                $scope.globalDoc.searchAddress = $scope.wrapper.address;
                GlobalsService.updateGlobalDoc($scope.globalDoc);
            }

            if ($scope.wrapper.address) {
                // Se rellena el objeto para la búsqueda con la información obtenida
                var searchObject = {};
                // Primero se intenta buscar por CP, si no lo obtiene, por ciudad
                searchObject.lat = $scope.wrapper.address.Latitude;
                searchObject.lng = $scope.wrapper.address.Longitude;
                searchObject.type = $scope.category;

                // Se muestra un 'cargando..' mientras se recuperan los datos y se pasa de vista
                $ionicLoading.show({
                    template: 'Cargando...'
                });
                Business.getListaNegocios(searchObject)
                    .then(function (response) {
                        var res = response.data.resultado;
                        if (res) {
                            if (res.code == 0) {
                                $state.go('app.search-result', {
                                    searchResult: res
                                });
                            } else if (res.code == -3) {
                                $ionicLoading.hide();
                                // Token incorrecto
                                User.refreshData();
                            } else {
                                console.log('KO LISTADO');
                                $ionicLoading.hide();
                                $cordovaDialogs.alert('Lo sentimos, no tenemos ningún restaurante en esa zona', 'Buscando negocios..', 'OK');
                            }
                        }
                    }, function () {
                        $ionicLoading.hide();
                        console.log('getListaNegocios ERROR');
                        $cordovaDialogs.alert('Lo sentimos, no tenemos ningún restaurante en esa zona', 'Buscando negocios..', 'OK');
                    });
            }

        });
    });

    // Cada vez que se entra en la vista principal se comprueba si el usuario tiene direcciones habituales y se "limpian"
    $scope.$on('$ionicView.enter', function() {
        if ($scope.globalDoc.user && $scope.globalDoc.user.direcciones) {
            // Si está logeado se muestra un selector con las direcciones de entrega habituales
            $scope.geoAddresses = angular.copy($scope.globalDoc.user.direcciones);
            removeOldAddresses();
        }
    });

    // Objeto que encapsula el valor del 'place' devuelto por la API de Google Places
    $scope.wrapper = {};
    // Seleccionada opción restaurantes por defecto
    $scope.findWhat = 'BUSCA RESTAURANTES';
    $scope.category = 0;
    $scope.showAutocompleteInputWhenLogged = false;

    $scope.image = 'url(img/findrest.jpg)';
    $scope.images = [
        'url(img/repartidor-bici.jpg)',
        'url(img/find_rest.png)'
    ];

    $scope.toggleAutocompleteVisibility = function () {
        $scope.showAutocompleteInputWhenLogged = !$scope.showAutocompleteInputWhenLogged;
    };

    $scope.addressChange = function (address) {
        console.log('address 2 : ' + JSON.stringify(address));
        // Se comprueba que venga el parámetro address, es decir, que no haya clicado en 'Selecciona una dirección'
        if (address) {
            // Se actualiza la dirección actual y la de búsqueda (hacer copy?)
            $scope.globalDoc.currentAddress = address;

            if (!$scope.globalDoc.searchAddress) {
                $scope.globalDoc.searchAddress = {};
            }
            $scope.globalDoc.searchAddress.Street = address.address;
            $scope.globalDoc.searchAddress.Latitude = address.lat;
            $scope.globalDoc.searchAddress.Longitude = address.lng;

            GlobalsService.updateGlobalDoc($scope.globalDoc);

            // TODO: Actualizar el valor del input y el del objeto address?
            // Actualizar el valor del input y el del objeto address
            document.getElementById('google_places_ac').value = address.address;
            $scope.wrapper.address = {};
            $scope.wrapper.address.Value = address.address;
            $scope.wrapper.address.Street = address.address;
            $scope.wrapper.address.Latitude = address.lat;
            $scope.wrapper.address.Longitude = address.lng;
/*
            // Se rellena el objeto para la búsqueda con la información obtenida
            var searchObject = {};

            searchObject.lat = address.lat;
            searchObject.lng = address.lng;
            searchObject.type = $scope.category;

            $ionicLoading.show({
                template: 'Cargando...'
            });
            Business.getListaNegocios(searchObject)
                .then(function (response) {
                    var res = response.data.resultado;
                    if (res) {
                        if (res.code == 0) {
                            $state.go('app.search-result', {
                                searchResult: res
                            });
                        } else if (res.code == -3) {
                            $ionicLoading.hide();
                            // Token incorrecto
                            User.refreshData();
                        } else {
                            console.log('KO LISTADO');
                            $ionicLoading.hide();
                            $cordovaDialogs.alert('Lo sentimos, no tenemos ningún restaurante en esa zona', 'Buscando negocios..', 'OK');
                        }
                    }
                }, function () {
                    $ionicLoading.hide();
                    console.log('getListaNegocios ERROR');
                    $cordovaDialogs.alert('Lo sentimos, no tenemos ningún restaurante en esa zona', 'Buscando negocios..', 'OK');
                });
*/
        }
    };

    // Cambio de imágenes de fondo
    // TODO: hacer transición suave
    /*
    $interval(function() {
        if($scope.image == $scope.images[0]) {
            $scope.image = $scope.images[1];
        } else {
            $scope.image = $scope.images[0];
        }
    }, 4000, 0);
    */

    $scope.getCurrentLocation = function () {
        // Se muestra un 'cargando..' mientras se recupera la posición
        $ionicLoading.show({
            template: 'Obteniendo ubicación...'
        });

        var posOptions = {timeout: 10000, enableHighAccuracy: false};
        $cordovaGeolocation.getCurrentPosition(posOptions)
            .then(function (position) {
                var lat  = position.coords.latitude;
                var long = position.coords.longitude;

                var geocoder = new google.maps.Geocoder();
                var latlng = new google.maps.LatLng(lat, long);
                var request = {
                    latLng: latlng
                };
                geocoder.geocode(request, function(data, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (data[0] != null) {
                            var postCode = null;
                            var city = null;
                            var street = null;

                            // Buscar calle, población y código postal
                            for(var i = 0; i < data[0].address_components.length; i++) {
                                var component = data[0].address_components[i];

                                if (component.types[0] == "postal_code") {
                                    postCode = component.long_name;
                                }
                                if(component.types[0] == "locality") {
                                    city = component.long_name;
                                }
                                if(component.types[0] == "route") {
                                    street = component.long_name;
                                }
                            }

                            // Actualizar dirección de búsqueda
                            // Si aún no está creado el objeto para guardar la dirección de búsqueda se crea
                            if (!$scope.globalDoc.searchAddress) {
                                $scope.globalDoc.searchAddress = {};
                            }
                            $scope.globalDoc.searchAddress.City = city;
                            $scope.globalDoc.searchAddress.PostCode = postCode;
                            $scope.globalDoc.searchAddress.Street = street;
                            $scope.globalDoc.searchAddress.Latitude = position.coords.latitude;
                            $scope.globalDoc.searchAddress.Longitude = position.coords.longitude;
                            GlobalsService.updateGlobalDoc($scope.globalDoc);

                            // Actualizar el valor del input y el del objeto address
                            document.getElementById('google_places_ac').value = data[0].formatted_address;
                            $scope.wrapper.address = {};
                            $scope.wrapper.address.City = city;
                            $scope.wrapper.address.PostCode = postCode;
                            $scope.wrapper.address.Value = street;
                            $scope.wrapper.address.Street = street;
                            $scope.wrapper.address.Latitude = position.coords.latitude;
                            $scope.wrapper.address.Longitude = position.coords.longitude;

                            $ionicLoading.hide();
/*
                            // Se rellena el objeto para la búsqueda con la información obtenida
                            var searchObject = {};
                            searchObject.lat = position.coords.latitude;
                            searchObject.lng = position.coords.longitude;
                            searchObject.type = $scope.category;

                            $ionicLoading.show({
                                template: 'Cargando...'
                            });
                            Business.getListaNegocios(searchObject)
                                .then(function (response) {
                                    var res = response.data.resultado;
                                    if (res) {
                                        if (res.code == 0) {
                                            $state.go('app.search-result', {
                                                searchResult: res
                                            });
                                        } else if (res.code == -3) {
                                            $ionicLoading.hide();
                                            // Token incorrecto
                                            User.refreshData();
                                        } else {
                                            console.log('KO LISTADO');
                                            $ionicLoading.hide();
                                            $cordovaDialogs.alert('Lo sentimos, no tenemos ningún restaurante en esa zona', 'Buscando negocios..', 'OK');
                                        }
                                    }
                                }, function () {
                                    $ionicLoading.hide();
                                    console.log('getListaNegocios ERROR');
                                    $cordovaDialogs.alert('Lo sentimos, no tenemos ningún restaurante en esa zona', 'Buscando negocios..', 'OK');
                                });
*/
                        } else {
                            console.log("No address available");
                        }
                    }
                });


            }, function(err) {
                $ionicLoading.hide();

                $cordovaDialogs.confirm('Es necesario que permitas el acceso a tu ubicación para usar esta función. Puedes hacerlo desde ajustes',
                    'Acceso a ubicación actual', ['Ir a Ajustes', 'Cancelar'])
                    .then(function(buttonIndex) {
                        // Si escoge Ir a Ajustes
                        if (buttonIndex == 1) {
                            try {
                                if (ionic.Platform.isIOS()) {
                                    cordova.plugins.diagnostic.switchToSettings();
                                } else {
                                    cordova.plugins.diagnostic.switchToLocationSettings();
                                }
                            } catch (err) {}
                        }
                        $ionicLoading.hide();
                    });
            });
    };

    $scope.find = function(type) {
        $scope.category = type;
        if (type == 1) {
            $scope.findWhat = 'BUSCA RESTAURANTES';
        } else if (type == 2) {
            $scope.findWhat = 'BUSCA PRODUCTOS';
        }
    };

    $scope.searchResult = function () {
        console.log('direccion 1 : ' + $scope.wrapper.address);
        var searchObject = {};
        // Primero se intenta buscar por CP, si no lo obtiene, por ciudad
        searchObject.lat = $scope.wrapper.address.Latitude;
        searchObject.lng = $scope.wrapper.address.Longitude;
        searchObject.type = $scope.category;

        // Se muestra un 'cargando..' mientras se recuperan los datos y se pasa de vista
        $ionicLoading.show({
            template: 'Cargando...'
        });
        Business.getListaNegocios(searchObject)
            .then(function (response) {
                var res = response.data.resultado;
                if (res) {
                    if (res.code == 0) {
                        $state.go('app.search-result', {
                            searchResult: res
                        });
                    } else if (res.code == -3) {
                        $ionicLoading.hide();
                        // Token incorrecto
                        User.refreshData();
                    } else {
                        console.log('KO LISTADO');
                        $ionicLoading.hide();
                        $cordovaDialogs.alert('Lo sentimos, no tenemos ningún restaurante en esa zona', 'Buscando negocios..', 'OK');
                    }
                }
            }, function () {
                $ionicLoading.hide();
                console.log('getListaNegocios ERROR');
                $cordovaDialogs.alert('Lo sentimos, no tenemos ningún restaurante en esa zona', 'Buscando negocios..', 'OK');
            });
    };

    $scope.closeWhatsAppPopup = function () {
        $scope.myWhatsAppPopup.close();
    };

    $scope.checkWhatsappNumber = function() {
        if ($scope.globalDoc.selectedCountry.code == 'ec') {
            return '5930962640056';
        } else if ($scope.globalDoc.selectedCountry.code == 'es') {
            return '34672309164';
        } else if ($scope.globalDoc.selectedCountry.code == 'pa') {
            return '50760607478';
        } 
    };

    $scope.returnUrl = function() {
        return 'whatsapp://send?phone=+584149913127&text=message';
    }

    $scope.copyWhatsApp = function (number) {
        $cordovaClipboard
            .copy(number)
            .then(function () {
                $ionicLoading.show({
                    template: 'Nº copiado al portapapeles'
                });
                $timeout(function() {
                    $ionicLoading.hide();
                }, 2000);
            });
    };

    $scope.openWhatsApp = function () {
        $scope.myWhatsAppPopup.close();
        //window.open("'whatsapp://send?phone=+584149913127&text=message'", "_system");
        var number;
        var link = 'whatsapp://send?phone=+' + $scope.checkWhatsappNumber()+ '&text=message';
        console.log('link ' + link);
        window.location.href = link;
    };

    $scope.whatsappPopup = function() {
        $scope.myWhatsAppPopup = $ionicPopup.show({
            templateUrl: 'templates/whatsapp-order-popup.html',
            cssClass: 'whatsapp-popup',
            title: '¿Ya sabes qué pedir?',
            subTitle: 'Pide más rápido y más fácil por WhatsApp',
            scope: $scope,
            buttons: [
            ]
        });
    };

    $scope.countryChange = function (country) {
        console.log(country);
    };

    $scope.confirmCountry = function () {
        console.log($scope.selectedCountry.country);
        // Se comprueba que venga el parámetro country, es decir, que no haya clicado en 'Selecciona un país'
        if ($scope.selectedCountry.country) {
            $scope.globalDoc.countryIsSelected = true;
            // Se actualiza el país actual
            $scope.globalDoc.selectedCountry = $scope.selectedCountry.country;
            GlobalsService.updateGlobalDoc($scope.globalDoc);  
            // Se define el locale a usar
            tmhDynamicLocale.set($scope.selectedCountry.country.completeCode);
            // Se vuelve a llamar al ws para obtener la info de contacto correcto
            Business.getContacto()
                .then(function (response) {
                    var res = response.data.resultado;
                    if (res.code == 0) {
                        $scope.globalDoc.contacto = res.contacto;
                        GlobalsService.updateGlobalDoc($scope.globalDoc);
                    }
                });
        }
    };

    // Función que se dispara cuando gana el foco el input, para deshabilitar el 'data-tap-disabled' de Ionic
    // que hace que el Autocomplete de Google Places no funcione bien en iOS
    $scope.disableTap = function() {
        container = document.getElementsByClassName('pac-container');
        // disable ionic data tab
        angular.element(container).attr('data-tap-disabled', 'true');
        // leave input field if google-address-entry is selected
        angular.element(container).on("click", function(){
            document.getElementById('searchBar').blur();
        });
    };
})

.controller('SearchResultCtrl', function($scope, $ionicPopover, $ionicFilterBar, $stateParams, Business, $state,
                                         $ionicLoading, $cordovaDialogs, User, GlobalsService, $ionicHistory, currencySettings) {
    $scope.$on('$ionicView.enter', function() {
        $ionicLoading.hide();
    });

    // Se obtiene el documento de la BD donde persistimos datos de la app
    var globalDoc = GlobalsService.getGlobalDoc();
    // Se inyecta al scope las constantes de la moneda
    $scope.currencySettings = currencySettings;

    // TODO: comprobar que tengo resultados de restaurantes
    $scope.searchResult = $stateParams.searchResult;
    $scope.pedido = User.pedido;

    // Objetos para almacenar los filtros de los restaurantes
    $scope.filtersOpen = {};
    $scope.filtersOpen.abierto = 1;
    $scope.filtersClosed = {};
    $scope.filtersClosed.abierto = 0;

    // Inicialmente 'Todos' estará seleccionado en el filtro de tipo cocina
    $scope.todosTiposSelected = true;

    $ionicPopover.fromTemplateUrl('templates/filtro-tipos-cocina.html', {
        scope: $scope,
    }).then(function(popover) {
        $scope.popover = popover;
    });

    $scope.orderTipoCocinaByTotal = function(tipoCocina){
        return parseInt(tipoCocina.total);
    };

    $scope.filterTipoCocina = function (tipoCocina, tiposCocinaArray) {
        // Se activa el filtro para ese tipo de comida
        $scope.filtersClosed.tipos_cocina = { "id": tipoCocina.id_tipo_cocina};
        $scope.filtersOpen.tipos_cocina = {"id": tipoCocina.id_tipo_cocina};

        // Se marca para que quede activa en el listado y se resetean las que pudiesen estar marcadas anteriormente
        angular.forEach(tiposCocinaArray, function (tipoCocinaObj) {
            tipoCocinaObj.selected = false;
        });
        tipoCocina.selected = true;
        $scope.todosTiposSelected = false;
    };

    $scope.resetFilterTipoCocina = function (tiposCocinaArray) {
        if ($scope.filtersClosed.tipos_cocina) {
            delete $scope.filtersClosed.tipos_cocina;
        }

        if ($scope.filtersOpen.tipos_cocina) {
            delete $scope.filtersOpen.tipos_cocina;
        }
        // Se marca para que quede activa en el listado y se resetean las que pudiesen estar marcadas anteriormente
        angular.forEach(tiposCocinaArray, function (tipoCocinaObj) {
            tipoCocinaObj.selected = false;
        });
        $scope.todosTiposSelected = true;
    };

    // Para la header de filtrado en búsqueda de restaurantes que aparece en la cabecera
    var filterBarInstance;
    $scope.showFilterBar = function() {
        filterBarInstance = $ionicFilterBar.show({
            //items: $scope.items,
            update: function (filteredItems, filterText) {
                //$scope.items = filteredItems;
                // En lugar de pasarle el array de restaurantes y actualizarlo, simplemente se actualiza su filtro del
                // scope padre, tanto de los restaurantes cerrados como abiertos
                $scope.filtersOpen.nombre = filterText;
                $scope.filtersClosed.nombre = filterText;
            }
        });
    };

    $scope.favouriteOn = false;
    $scope.toggleFavourite = function(restaurant) {
        if (!User.auth.isLogged) {
            $cordovaDialogs.alert('Inicia sesión para guardar tus favoritos', 'Favoritos', 'OK');
        } else {
            var restaurantObj = {};
            restaurantObj.id_negocio = restaurant.id;

            // Si ya es favorito, hay que desmarcarlo como favorito, si no lo es, se marca como favorito
            if (restaurant.favorito) {
                restaurant.favorito = 0;
                User.setFavorito(restaurantObj)
                    .then(function (response) {
                        // TODO: mirar el código y dar feedback al usuario de que se actualizaron sus favoritos, pero sin
                        // ser invasivo con un ionicLoading, algo más como una label, como los toasts de android y que desaparezca

                        var res = response.data.resultado;
                        if (res) {
                            if (res.code == 1 || res.code == 2) {
                                // Se actualizan los favoritos
                                User.getFavoritos()
                                    .then(function (response) {
                                        var res = response.data.resultado;
                                        if (res.code == 0) {
                                            //var globalDoc = GlobalsService.getGlobalDoc();
                                            globalDoc.user.favoritos = res.favoritos;
                                            GlobalsService.updateGlobalDoc(globalDoc);
                                        }
                                    });
                            }
                        }
                    });
            } else {
                restaurant.favorito = 1;
                User.setFavorito(restaurantObj)
                    .then(function (response) {
                        // TODO: mirar el código y dar feedback al usuario de que se actualizaron sus favoritos, pero sin
                        // ser invasivo con un ionicLoading, algo más como una label, como los toasts de android y que desaparezca

                        var res = response.data.resultado;
                        if (res) {
                            if (res.code == 1 || res.code == 2) {
                                // Se actualizan los favoritos
                                User.getFavoritos()
                                    .then(function (response) {
                                        var res = response.data.resultado;
                                        if (res.code == 0) {
                                            //var globalDoc = GlobalsService.getGlobalDoc();
                                            globalDoc.user.favoritos = res.favoritos;
                                            GlobalsService.updateGlobalDoc(globalDoc);
                                        }
                                    });
                            }
                        }
                    });
            }
        }
    };

    $scope.goToBusiness = function(business) {
        console.dir(globalDoc.searchAddress);
        var businessObject = {};
        businessObject.negocio = business.id;
        businessObject.lat = globalDoc.searchAddress.Latitude;
        businessObject.lng = globalDoc.searchAddress.Longitude;

        // Se muestra un 'cargando..' mientras se recuperan los datos y se pasa de vista
        $ionicLoading.show({
            template: 'Cargando...'
        });
        Business.getDatosRestaurante(businessObject)
            .then(function (response) {
                var res = response.data.resultado;
                if (res) {
                    if (res.code == 0) {
                        $state.go('app.restaurant', {
                            businessResult: res
                        });
                    } else if (res.code == -3) {
                        $ionicLoading.hide();
                        // Token incorrecto
                        User.refreshData();
                    } else {
                        console.log('KO ir a restaurante');
                        $ionicLoading.hide();
                        $cordovaDialogs.alert('Servicio temporalmente no disponible', 'Accediendo al restaurante', 'OK');
                    }
                }
            }, function () {
                $ionicLoading.hide();
                console.log('getDatosRestaurante ERROR');
                $cordovaDialogs.alert('Servicio temporalmente no disponible', 'Accediendo al restaurante', 'OK');
            });
    };

})

.controller('LoginCtrl', function($scope, User, $cordovaDialogs, md5, $ionicLoading, $timeout, $state, GlobalsService,
                                  Business, $ionicHistory, $stateParams, $ionicPopup) {
    // Se recoge el parámetro para saber si el login fue invocado desde el menú o desde el carrito
    $scope.sourceView = $stateParams.sourceView;
    // Variable para guardar los campos del usuario en el login y registro
    $scope.user = {};
    // Variable para guardar los datos del popup para recuperar contraseña
    $scope.forgotPass = {};

    // Se obtiene el documento de la BD donde persistimos datos de la app
    var globalDoc = GlobalsService.getGlobalDoc();

    // Se vuelve atrás, en caso que no haya vista anterior, se va la 'home'
    $scope.goBack = function () {
        if ($ionicHistory.backView()) {
            $ionicHistory.goBack();
        } else {
            $state.go('app.find-rest');
        }
    };

    $scope.doLogIn = function () {
        var credentials = angular.copy($scope.user);
        // Se calcula el md5 de la contraseña para enviar al servidor
        credentials.password = md5.createHash($scope.user.password);

        $ionicLoading.show({
            template: 'Iniciando sesión..'
        });
        User.login(credentials)
            .then(function(response) {
                $ionicLoading.hide();

                var res = response.data.resultado;
                if (res) {
                    if (res.code == -1) {
                        $cordovaDialogs.alert('El usuario o contraseña son incorrectos', 'Inicio sesión', 'OK');
                    } else if (res.code == -2) {
                        $cordovaDialogs.alert('Servicio temporalmente no disponible', 'Inicio sesión', 'OK');
                    } else if (res.code == 0) {
                        // Se actualiza el usuario persistido en local con los datos recibidos del servidor
                        globalDoc.user = res;
                        // Se guarda la pass en local. ESTO ES UNA CHAMBONADA Y UN AGUJERO DE SEGURIDAD PERO CON LOS WS
                        // EXISTENTES SE NECESITA HACERLO
                        // TODO: quitar esta patraña cuando se pueda hacer
                        globalDoc.user.pass = credentials.password;

                        GlobalsService.updateGlobalDoc(globalDoc);
                        // Se marca el usuario como logeado y se guarda también el token en memoria
                        User.auth.isLogged = true;
                        User.auth.token = res.token;
                        Business.setAuthToken(res.token);

                        // Se actualizan los favoritos
                        User.getFavoritos()
                            .then(function (response) {
                                var res = response.data.resultado;
                                if (res.code == 0) {
                                    globalDoc.user.favoritos = res.favoritos;
                                    GlobalsService.updateGlobalDoc(globalDoc);
                                }
                            });
                        // Se actualiza el token de PUSH
                        User.setPush();

                        if ($scope.sourceView == 'cart') {
                            // Se marca la vista anterior como la vista actual justo antes de navegar a la siguiente,
                            // para quitar de la navegación la página de login y así conservar el volver atrás correcto
                            $ionicHistory.currentView($ionicHistory.backView());
                            $state.go('app.order-address');
                        } else {
                            $state.go('app.find-rest');
                        }
                    }
                }
            }, function () {
                console.log('LOGIN KO');
                $ionicLoading.hide();
                $cordovaDialogs.alert('Servicio temporalmente no disponible', 'Inicio sesión', 'OK');
            });
    };

    $scope.doRegister = function () {
        var credentials = angular.copy($scope.user);
        // Se calcula el md5 de la contraseña para enviar al servidor
        credentials.password = md5.createHash($scope.user.password);
        User.register(credentials)
            .then(function(response) {
                var res = response.data.resultado;
                if (res) {
                    if (res.code == -2) {
                        $cordovaDialogs.alert('Email de usuario repetido', 'Registro', 'OK');
                    } else if (res.code == -4) {
                        $cordovaDialogs.alert('Datos incompletos', 'Registro', 'OK');
                    } else if (res.code == 0) {
                        $ionicLoading.show({
                            template: 'Usuario registrado!'
                        });
                        // Guardar info de usuario y token devuelto
                        globalDoc.user.token = res.token;
                        globalDoc.user.email = credentials.username;
                        globalDoc.user.nombre = credentials.nombre;
                        globalDoc.user.apellidos = credentials.apellidos;
                        globalDoc.user.movil = credentials.movil;
                        globalDoc.user.direcciones = [];
                        globalDoc.user.descuentos = [];
                        globalDoc.user.favortios = [];
                        // Se guarda la pass en local. ESTO ES UNA CHAMBONADA Y UN AGUJERO DE SEGURIDAD PERO CON LOS WS
                        // EXISTENTES SE NECESITA HACERLO
                        // TODO: quitar esta patraña cuando se pueda hacer
                        globalDoc.user.pass = credentials.password;

                        GlobalsService.updateGlobalDoc(globalDoc);
                        // Se marca el usuario como logeado y se guarda también el token en memoria
                        User.auth.isLogged = true;
                        User.auth.token = res.token;
                        Business.setAuthToken(res.token);
                        // Se actualiza el token de PUSH
                        User.setPush();

                        // Al cabo de 1 segundo se redirige hacia la pantalla principal o al carrito
                        $timeout(function(){
                            $ionicLoading.hide();
                            if ($scope.sourceView == 'cart') {
                                $state.go('app.order-address');
                            } else {
                                $state.go('app.find-rest');
                            }
                        }, 1000);
                    }
                }
            }, function () {
                console.log('REGISTRO KO');
                $cordovaDialogs.alert('Servicio temporalmente no disponible', 'Registro', 'OK');
            });
    };

    $scope.recoverPass = function () {
        var forgotPassObj = {
            'email': $scope.forgotPass.email
        };

        $ionicLoading.show({
            template: 'Enviando...'
        });
        console.log(JSON.stringify(forgotPassObj));
        User.recoverPass(forgotPassObj)
            .then(function (response) {
                $ionicLoading.hide();
                var res = response.data.resultado;
                if (res) {
                    if (res.code == -1) {
                        $cordovaDialogs.alert('Email de usuario no encontrado', 'Recuperar contraseña', 'OK');
                    } else if (res.code == -4) {
                        $cordovaDialogs.alert('Datos incorrectos', 'Recuperar contraseña', 'OK');
                    } else if (res.code == 0) {
                        $scope.forgotPass.email = '';
                        $ionicLoading.show({
                            template: 'Contraseña enviada al correo!'
                        });
                        $timeout(function () {
                            $ionicLoading.hide();
                            $scope.forgotPassPopup.close();
                        }, 2000);
                    }

                }
            }, function (err) {
                $scope.forgotPass.email = '';
                $ionicLoading.hide();
            });
    };

    $scope.closeForgotPassPopup = function () {
        $scope.forgotPassPopup.close();
        $scope.forgotPass.email = '';
    };

    $scope.openForgotPassPopup = function() {
        $scope.forgotPassPopup = $ionicPopup.show({
            templateUrl: 'templates/forgot-pass-popup.html',
            cssClass: 'forgot-pass-popup',
            title: '¿Has olvidado tu contraseña?',
            subTitle: 'No te preocupes, a todos nos pasa',
            scope: $scope,
            buttons: [
            ]
        });
    };
})

.controller('RestaurantCtrl', function($scope, $stateParams, User, $ionicHistory, $state, $cordovaDialogs, $ionicModal,
                                       Business, $ionicLoading, $rootScope, currencySettings, GlobalsService) {
    // Se espera al evento 'enter' para cerrar el loading porque queda mejor el efecto para restaurantes con cartas amplias
    $scope.$on('$ionicView.enter', function() {
        $ionicLoading.hide();
    });

    var globalDoc = GlobalsService.getGlobalDoc();

    // Se inyecta al scope las constantes de la moneda
    $scope.currencySettings = currencySettings;

    // Se reciben los parámetros enviados a la vista
    $scope.negocio = $stateParams.businessResult.negocio;
    $scope.carta = $stateParams.businessResult.carta;

    $rootScope.negocios_mixing = $scope.negocio.negocios_mixing;

    // Se obtiene el pedido en curso
    $scope.pedido = User.pedido;

    // Índice del restaurante para gestionar el tema del mixin
    $scope.negocioIndex = 0;

    // Se busca si algún producto de este negocio está incluido en el pedido
    var negocioInPedidoResult = User.getNegocioInPedidoById($scope.negocio.id);
    if (negocioInPedidoResult) {
        $scope.pedidoRestaurante = negocioInPedidoResult[0];
        $scope.negocioIndex = negocioInPedidoResult[1];
    } else {
        // Se crea el contenedor del pedido para este restaurante y se inicializa
        $scope.pedidoRestaurante = {
            productos: []
        };
        $scope.pedidoRestaurante.id_negocio = $scope.negocio.id;
        $scope.pedidoRestaurante.nombre = $scope.negocio.nombre;
        //$scope.pedidoRestaurante.transporte = Business.getTransportCost($scope.negocio.ctrans);
        $scope.pedidoRestaurante.transporte = $scope.negocio.transport;
        console.log($scope.pedidoRestaurante.transporte);
        $scope.pedidoRestaurante.transporteFloat = parseFloat($scope.pedidoRestaurante.transporte);
        console.log($scope.pedidoRestaurante.transporteFloat);
        $scope.pedidoRestaurante.total_restaurante = 0;
        //$scope.pedidoRestaurante.cps = $scope.negocio.cps;
        //$scope.pedidoRestaurante.ctrans = $scope.negocio.ctrans;
        //$scope.pedidoRestaurante.pedido_minimo = $scope.negocio.pedido_minimo;
        $scope.pedidoRestaurante.pedido_minimo = $scope.negocio.minorder;
        $scope.pedidoRestaurante.free = $scope.negocio.free;
        $scope.pedidoRestaurante.abierto = $scope.negocio.abierto;
        $scope.pedidoRestaurante.lista_horarios = $scope.negocio.lista_horarios;

        // Se añade el restaurante al pedido y se obtiene su id restando 1 a la longitud actual
        $scope.negocioIndex = User.pedido.pedido.push($scope.pedidoRestaurante) - 1;
    }

    $ionicModal.fromTemplateUrl('templates/mixing-suggestion.html', {
        scope: $rootScope,
        animation: 'slide-in-up',
        backdropClickToClose: false,
        hardwareBackButtonClose: false
    }).then(function(modal) {
        $rootScope.mixingModal = modal;
    });

    $scope.isCustomizable = function (producto) {
        return producto.ingredientes.length || producto.extras.length || producto.combinaciones.length;
    };

    $scope.showRes = function () {
        console.dir($scope.pedido);
        console.log(JSON.stringify($scope.pedido));
    };

    // Función que devuelve el índice del producto en el pedido
    $scope.getProductIndexInPedido = function (productId) {
        for (var i = 0; i < $scope.pedidoRestaurante.productos.length; i++) {
            if ($scope.pedidoRestaurante.productos[i].id_producto == productId) {
                // Se devuelve el índice del objeto en el array
                return i;
            }
        }
        // Si no se encuentra se devuelve -1
        return -1;
    };

    // Función que devuelve la cantidad total del producto en el pedido
    $scope.getProductQuantityInPedido = function (productId) {
        var productQuantity = 0;
        for (var i = 0; i < $scope.pedidoRestaurante.productos.length; i++) {
            if ($scope.pedidoRestaurante.productos[i].id_producto == productId) {
                productQuantity += $scope.pedidoRestaurante.productos[i].cantidad;
            }
        }
        return productQuantity;
    };

    // Función que borra todos los items de un producto en el pedido
    var cleanProductInPedido = function (productId) {
        for (var i = $scope.pedidoRestaurante.productos.length; i > 0; i--) {
            if ($scope.pedidoRestaurante.productos[i - 1].id_producto == productId) {
                $scope.pedidoRestaurante.productos.splice(i -1, 1);
            }
        }
    };

    /*
    * if given group is the selected group, deselect it
    * else, select the given group
    */
    $scope.toggleGroup = function(group) {
        if ($scope.isGroupShown(group)) {
            $scope.shownGroup = null;
        } else {
            $scope.shownGroup = group;
        }
    };

    $scope.isGroupShown = function(group) {
        return $scope.shownGroup === group;
    };

    // Función que gestiona el quitar productos del pedido
    $scope.removeFromCart = function(product) {
        // En caso de ser customizable se eliminan todos los items de este producto, si no se pueden borrar uno a uno
        if ($scope.isCustomizable(product)) {
            cleanProductInPedido(product.id);
        } else {
            // Se obtiene el array que encapsula el producto y el índice del mismo en el array global
            var result = User.getProductInPedidoById($scope.negocioIndex, product.id);
            if (result[0].cantidad == 1) {
                // Si la cantidad anterior era 1, se elimina el producto del array
                $scope.pedidoRestaurante.productos.splice(result[1], 1);
            } else {
                result[0].cantidad = result[0].cantidad - 1;
                // Se actualiza el importe total de este restaurante al saltar el watch de '$scope.pedidoRestaurante.productos'
            }
        }
    };

    // Función que gestiona el añadir productos del pedido
    $scope.addToCart = function (product, group, negocioIndex) {
        // En caso de ser customizable no se añade directamente al pedido, se va a sus detalles de producto
        if ($scope.isCustomizable(product)) {
            $ionicLoading.show({
                template: 'Cargando...'
            });
            $state.go('app.dish-details', {
                producto: product,
                negocioIndex: negocioIndex
            });
        } else {
            // TODO: hacer bien el tema de marcar un producto de un grupo añadido al pedido
            group.addedToCart = true;
            // Se obtiene el array que encapsula el producto y el índice del mismo en el array 'global'
            var result = User.getProductInPedidoById($scope.negocioIndex, product.id);

            // Si se obtiene un resultado significa que este producto ya está añadido al pedido, por lo que se incrementa su cantidad
            if (result) {
                result[0].cantidad = result[0].cantidad + 1;
                // Se actualiza el importe total de este restaurante al saltar el watch de '$scope.pedidoRestaurante.productos'
            } else {
                // Si no había sido previamente añadido se inicializa y añade al array 'global'
                var productObj = {};
                productObj.id_producto = product.id;
                productObj.nombre = product.nombre;
                productObj.cantidad = 1;
                // TODO: de donde se saca el tipo?
                productObj.tipo = "1";
                productObj.importe = product.precio;
                productObj.observaciones = "";
                productObj.ingredientes = [];
                productObj.extras = [];
                productObj.combinaciones = [];

                if (product.combinaciones.length > 0) {
                    // TODO: si tiene combinaciones hay que elegirle la primera!
                }

                $scope.pedidoRestaurante.productos.push(productObj);
            }
        }
    };

    var updateTotalRestaurant = function (productsInRestaurant) {
        var totalImporteProductos = 0;
        // Por cada producto se actualiza el total del importe de este negocio
        for(var i = 0; i < productsInRestaurant.length; i++) {
            totalImporteProductos = totalImporteProductos + (productsInRestaurant[i].importe * productsInRestaurant[i].cantidad);
        }

        // Se actualiza el total de este negocio/restaurante
        if ($scope.pedidoRestaurante.productos.length > 0) {
            $scope.pedidoRestaurante.total_restaurante = totalImporteProductos + $scope.pedidoRestaurante.transporteFloat;
        }

        // Se actualiza el total del pedido entero
        User.updateTotalPedido();
    };

    // Función para volver atrás, como a esta vista se puede llegar desde un nested state por la vía menu o directamente
    // desde favoritos, se esconde el ion-nav-back-button en esta vista, y se mete manualmente el icono
    $scope.goBack = function () {
        // Si hay productos añadidos, se comprueba si este restaurante permite mixing
        if ($scope.pedido.total_pedido > 0) {
            if ($scope.negocio.mixing == "0") {
                $cordovaDialogs.confirm("Este restaurante no permite hacer mixing. Si continuas se eliminará el pedido actual."
                    , 'Mixing delivery', ['Volver a búsqueda', 'Seguir en el restaurante'])
                    .then(function (buttonIndex) {
                        if (buttonIndex == 1) {
                            User.clearPedido();
                            $ionicHistory.goBack();
                        }
                    });
            } else if ($scope.negocio.mixing == "1" && $scope.negocio.negocios_mixing.length == 0) {
                // Si permite el mixing pero no tiene ninguno para hacer mixing.
                $cordovaDialogs.confirm("Actualmente este restaurante no permite hacer mixing. Si continuas se eliminará el pedido actual."
                    , 'Mixing delivery', ['Volver a búsqueda', 'Seguir en el restaurante'])
                    .then(function (buttonIndex) {
                        if (buttonIndex == 1) {
                            User.clearPedido();
                            $ionicHistory.goBack();
                        }
                    });
            } else {
                // Mixin y hay negocios
                $ionicHistory.goBack();
                $rootScope.mixingModal.show();
            }
        } else {
            // No hay productos añadidos
            User.clearPedido();
            $ionicHistory.goBack();
        }
    };

    $rootScope.finishMixingSelection = function (showConfirmation) {
        if (showConfirmation) {
            $cordovaDialogs.confirm("Si continuas se eliminará el pedido actual."
                , 'Mixing delivery', ['Vaciar pedido y volver a buscar', 'Cancelar'])
                .then(function (buttonIndex) {
                    if (buttonIndex == 1) {
                        User.clearPedido();
                        $rootScope.mixingModal.hide();
                    }
                });
        } else {
            User.clearPedido();
            $rootScope.mixingModal.hide();
        }

    };

    $rootScope.goToBusiness = function(business) {
        $rootScope.mixingModal.hide();
        var businessObject = {};
        businessObject.negocio = business.id_negocio;
        businessObject.lat = globalDoc.searchAddress.Latitude;
        businessObject.lng = globalDoc.searchAddress.Longitude;

        // Se muestra un 'cargando..' mientras se recuperan los datos y se pasa de vista
        $ionicLoading.show({
            template: 'Cargando...'
        });
        Business.getDatosRestaurante(businessObject)
            .then(function (response) {
                var res = response.data.resultado;
                if (res) {
                    if (res.code == 0) {
                        $state.go('app.restaurant', {
                            businessResult: res
                        });
                    } else if (res.code == -3) {
                        $ionicLoading.hide();
                        // Token incorrecto
                        User.refreshData();
                    } else {
                        console.log('KO ir a restaurante');
                        User.clearPedido();
                        $ionicLoading.hide();
                        $cordovaDialogs.alert('Servicio temporalmente no disponible', 'Accediendo al restaurante', 'OK');
                    }
                }
            }, function () {
                $ionicLoading.hide();
                console.log('getDatosRestaurante ERROR');
                User.clearPedido();
                $cordovaDialogs.alert('Servicio temporalmente no disponible', 'Accediendo al restaurante', 'OK');
            });
    };

    $scope.goToCart = function(totalPrice, minimumPrice) {
        // TODO: comprobar bien lo del transporte
        if (totalPrice >= minimumPrice) {
            $state.go('app.cart');
        }
    };

    $scope.goToDishDetails = function(producto, negocioIndex) {
        $ionicLoading.show({
            template: 'Cargando...'
        });
        $state.go('app.dish-details',
            {
                producto: producto,
                negocioIndex: negocioIndex
            }
        );
    };
/*
    $scope.$watchCollection('pedidoRestaurante.productos', function(newCollection) {
        updateTotalRestaurant(newCollection);
    });
*/
    $scope.$watch('pedidoRestaurante.productos', function(newCollection) {
        updateTotalRestaurant(newCollection);
    }, true);
})

.controller('RestaurantInfoCtrl', function($scope, $stateParams) {
    $scope.negocio = $stateParams.negocio;

    $scope.isToday = function (dayNumber) {
        var today = new Date();
        return today.getDay() == dayNumber;
    };
})

.controller('DishDetailsCtrl', function($scope, $stateParams, User, $ionicModal, $ionicHistory, $ionicLoading, currencySettings) {
    $scope.$on('$ionicView.enter', function() {
        $ionicLoading.hide();
    });

    // Se inyecta al scope las constantes de la moneda
    $scope.currencySettings = currencySettings;

    // Se obtienen los parámetros enviados a la vista
    $scope.producto = $stateParams.producto;
    var negocioIndex = $stateParams.negocioIndex;
    // Flags para comprobar si el producto ya estaba añadido al pedido y/o se añade en esta vista
    var alreadyAddedToCart = false;
    var addedInThisView = false;
    // Para guardar una copia de la cantidad original del producto en el pedido
    var alreadyAddedProductQuantity = 0;

    $scope.isCustomizable = $scope.producto.ingredientes.length || $scope.producto.extras.length ||
        $scope.producto.combinaciones.length;

    $scope.totalExtrasPrice = 0;
    // Se obtiene el pedido en curso
    $scope.pedido = User.pedido;

    var updateProductoImporte = function () {
        var importeTotal = parseFloat($scope.producto.precio);
        var totalExtras = 0;

        // Se recorren las combinaciones en busca de selecciones que conlleven aumento de precio
        for (var i = 0; i < $scope.productoInPedido.combinaciones.length; i++) {
            if (parseFloat($scope.productoInPedido.combinaciones[i].precio) > 0) {
                importeTotal = importeTotal + parseFloat($scope.productoInPedido.combinaciones[i].precio);
            }
        }

        // Se recorren los extras en busca de selecciones que conlleven aumento de precio
        for (var i = 0; i < $scope.productoInPedido.extras.length; i++) {
            if (parseFloat($scope.productoInPedido.extras[i].precio) > 0) {
                importeTotal = importeTotal + parseFloat($scope.productoInPedido.extras[i].precio);
                totalExtras = totalExtras + parseFloat($scope.productoInPedido.extras[i].precio);
            }
        }

        $scope.totalExtrasPrice = totalExtras.toFixed(2);
        $scope.productoInPedido.importe = importeTotal.toFixed(2);
    };

    $scope.showRes = function () {
        //console.log(User.pedido);
        //console.log($scope.productoInPedido);
    };

    // Se definen la ventanas modales que servirán para seleccionar extras y quitar ingredientes
    $ionicModal.fromTemplateUrl('templates/extras-selection.html', {
        scope: $scope,
        animation: 'slide-in-right'
    }).then(function(modal) {
        $scope.extrasModal = modal;
    });

    $ionicModal.fromTemplateUrl('templates/ingredientes-selection.html', {
        scope: $scope,
        animation: 'slide-in-right'
    }).then(function(modal) {
        $scope.ingredientesModal = modal;
    });

    // Si es customizable no se busca el producto en el pedido para trabajar sobre ese objeto, se crea siempre uno nuevo
    if (!$scope.isCustomizable) {
        // Se obtiene el producto añadido al pedido en caso que lo hubiese
        var result = User.getProductInPedidoById(negocioIndex, $scope.producto.id);
        if (result) {
            alreadyAddedToCart = true;
            alreadyAddedProductQuantity = result[0].cantidad;
            // Se guarda el producto y su índice en el pedido para usar en la vista
            $scope.productoInPedido = result[0];
            $scope.productoIndexInPedido = result[1];
        }
    }
    // Si es customizable o no existía previamente el producto en el pedido, se crea uno nuevo
    if ($scope.isCustomizable || !result) {
        // Si no había sido previamente añadido se inicializa y añade al array 'global'
        $scope.productoInPedido = {};
        $scope.productoInPedido.id_producto = $scope.producto.id;
        $scope.productoInPedido.nombre = $scope.producto.nombre;
        $scope.productoInPedido.cantidad = 1;
        // TODO: de donde se saca el tipo?
        $scope.productoInPedido.tipo = "1";
        $scope.productoInPedido.importe = parseFloat($scope.producto.precio).toFixed(2);
        $scope.productoInPedido.observaciones = "";
        $scope.productoInPedido.ingredientes = [];
        $scope.productoInPedido.extras = [];
        $scope.productoInPedido.combinaciones = [];

        // En caso de haber combinaciones se selecciona por defecto automáticamente la 1ª opción
        angular.forEach($scope.producto.combinaciones, function (combinacion) {
            if (!combinacion.valor) {
                // Se le añade a la propiedad 'valor' (que será el modelo enlazado en el select) la 1ª opción
                combinacion.valor = combinacion.valores[0];
                // Se construye el objeto para añadirlo al array de combinaciones del pedido
                var combinacionObj = {};
                combinacionObj.id = combinacion.id;
                combinacionObj.id_valor = combinacion.valores[0].id_valor;
                combinacionObj.nombre = combinacion.valores[0].valor;
                combinacionObj.precio = combinacion.valores[0].precio;
                $scope.productoInPedido.combinaciones.push(combinacionObj);
            }
        });
    }

    $scope.increaseProductQuantity = function () {
        $scope.productoInPedido.cantidad = $scope.productoInPedido.cantidad + 1;
    };

    $scope.decreaseProductQuantity = function () {
        if ($scope.productoInPedido.cantidad > 0) {
            $scope.productoInPedido.cantidad = $scope.productoInPedido.cantidad - 1;
        }
    };

    $scope.combinacionChange = function (combinacion) {
        // Se construye el objeto para añadirlo al array de combinaciones del pedido
        var combinacionObj = {};
        combinacionObj.id = combinacion.id;
        combinacionObj.id_valor = combinacion.valor.id_valor;
        combinacionObj.nombre = combinacion.valor.valor;
        combinacionObj.precio = combinacion.valor.precio;

        // Si se encuentra esa combinación ya seleccionada se actualiza el objeto
        for (var i = 0; i < $scope.productoInPedido.combinaciones.length; i++) {
            if ($scope.productoInPedido.combinaciones[i].id == combinacionObj.id) {
                $scope.productoInPedido.combinaciones[i] = combinacionObj;

                // Si la selección de la combinación conlleva un incremento de precio se actualiza el importe del producto
                if (parseFloat(combinacionObj.precio) > 0) {
                    updateProductoImporte();
                }

                return;
            }
        }
        // Si no, se añade
        $scope.productoInPedido.combinaciones.push(combinacionObj);
        // Si la selección de la combinación conlleva un incremento de precio se actualiza el importe del producto
        if (parseFloat(combinacionObj.precio) > 0) {
            updateProductoImporte();
        }
    };

    // Función para añadir/quitar extras al producto
    $scope.toggleExtra = function (extra) {
        if (extra.checked) {
            $scope.productoInPedido.extras.push(extra);
        } else {
            var index = $scope.productoInPedido.extras.indexOf(extra);
            $scope.productoInPedido.extras.splice(index, 1);
        }
        // Se actualiza el importe del producto
        updateProductoImporte();
    };

    // Función para añadir/quitar ingredientes al producto
    $scope.toggleIngrediente = function (ingrediente) {
        if (ingrediente.checked) {
            $scope.productoInPedido.ingredientes.push(ingrediente);
        } else {
            var index = $scope.productoInPedido.ingredientes.indexOf(ingrediente);
            $scope.productoInPedido.ingredientes.splice(index, 1);
        }
    };

    $scope.showEx = function () {
        //console.log($scope.producto.combinaciones);
        //console.log($scope.productoInPedido.combinaciones);
    };


    $scope.addToCart = function () {
        $ionicLoading.show({
            template: 'Añadiendo al pedido...'
        });

        // Si no había sido ya guardado se añade al pedido
        if (!alreadyAddedToCart) {
            // Se guarda el índice en el pedido para su posterior uso, como al eliminarlo
            $scope.productoIndexInPedido = User.pedido.pedido[negocioIndex].productos.push($scope.productoInPedido) -1;
        }

        addedInThisView = true;
        $ionicHistory.goBack();
    };

    // Función para limpiar las personalizaciones añadidas al producto
    var cleanCustomizations = function () {
        // Se limpian los extras seleccionados
        for (var i = $scope.producto.extras.length; i > 0; i--) {
            if ($scope.producto.extras[i - 1].checked) {
                $scope.producto.extras[i - 1].checked = false;
            }
        }
        // Se limpian los ingredientes seleccionados
        for (var i = $scope.producto.ingredientes.length; i > 0; i--) {
            if ($scope.producto.ingredientes[i - 1].checked) {
                $scope.producto.ingredientes[i - 1].checked = false;
            }
        }

        // Se limpian las combinaciones seleccionadas
        for (var i = $scope.producto.combinaciones.length; i > 0; i--) {
            if ($scope.producto.combinaciones[i - 1].valor) {
                delete $scope.producto.combinaciones[i - 1].valor;
            }
        }
    };

    // Antes de abandonar la vista, ejecutar una serie de tareas
    $scope.$on('$ionicView.beforeLeave', function() {
        // Si estaba añadido y se sale sin guardar el pedido, se restituye la cantidad original del producto
        if (alreadyAddedToCart && !addedInThisView) {
            $scope.productoInPedido.cantidad = alreadyAddedProductQuantity;
        }
        cleanCustomizations();
    });
})

.controller('OrderCartCtrl', function($scope, User, GlobalsService, $ionicHistory, $state, makeIdReferencia,
                                      $ionicViewSwitcher, $cordovaDialogs, $filter, $ionicLoading, $timeout, PaypalService, currencySettings, $ionicScrollDelegate, md5) {
    // Se inyecta al scope las constantes de la moneda
    $scope.currencySettings = currencySettings;
    // Se obtiene el pedido en curso
    $scope.pedido = User.pedido;
    console.log(User.pedido);
    // Se obtiene el coste del transporte total y si es variable o no
    $scope.totalTransportCost = User.getTotalOrderTransportCost();
    //$scope.isTransportCostVariable = User.isTransportCostVariable();
    // Se actualiza su referencia
    User.pedido.referencia = makeIdReferencia;
    $scope.globalDoc = GlobalsService.getGlobalDoc();
    // Se define la variable que guarda el valor de la forma de pago (por defecto, contrareembolso)
    $scope.paymentChoice = {
        value: 3
    };
    $scope.paypalApproved = false;

    // Variables para la gestión de cupones
    $scope.cuponValid = false;
    $scope.cuponDiscount = 0;
    $scope.cuponToValidate = '';
    $scope.cuponApplied = '';
    // Comentarios
    $scope.comments = '';

    // Función que comprueba si se alcanza el pedido mínimo en todos los negocios añadidos
    $scope.checkMinimumOrderPrice = function () {
        var productsFound = false;
        // Por cada negocio se comprueba si el total del restaurante es mayor o igual que el pedido mínimo requerido
        for (var i = 0; i < $scope.pedido.pedido.length; i++) {
            // Sólo se comprueban restaurantes que tengan productos añadidos
            if ($scope.pedido.pedido[i].productos.length > 0) {
                productsFound = true;
                if (($scope.pedido.pedido[i].total_restaurante - parseFloat($scope.pedido.pedido[i].transporte))  <
                parseFloat($scope.pedido.pedido[i].pedido_minimo)) {
                    return false;
                }
            }
        }
        // Si ninguno de los restaurantes tiene productos añadidos, no se alcanza el mínimo
        return productsFound;
    };

    $scope.showPedido = function () {
        console.log(User.pedido);
        console.log(JSON.stringify(User.pedido));
    };

    $scope.commentsChange = function (comment) {
        User.pedido.info_pedido_cliente.comentarios = comment;
    };

    $scope.removeCupon = function () {
        if (!$scope.paypalApproved) {
            $scope.cuponApplied = '';
            $scope.cuponValid = false;
            $scope.cuponDiscount = 0;
            User.pedido.descuento = '';
            User.pedido.info_pedido_cliente.id_descuento_cliente = '';
        }
    };

    $scope.validateCupon = function (cuponValue) {
        if (cuponValue && !$scope.paypalApproved) {
            var cuponObj = User.pedido;
            cuponObj.descuento = cuponValue;

            User.getCuponValido(cuponObj)
                .then(function (response) {
                    //TODO: gestión de errores
                    var res = response.data.resultado;
                    if (res) {
                        if (res.code == 0) {
                            $scope.cuponApplied = cuponValue;
                            // El descuento puede ser de una cantidad o de un porcentaje
                            if (parseFloat(res.cantidad) > 0) {
                                $scope.cuponDiscount = res.cantidad;
                            } else {
                                $scope.cuponDiscount = (User.pedido.total_pedido - $scope.totalTransportCost) * (parseFloat(res.porcentaje)/100);
                            }

                            $scope.cuponToValidate = '';
                            $scope.cuponValid = true;
                            User.pedido.descuento = cuponValue;
                            User.pedido.info_pedido_cliente.id_descuento_cliente = res.id_descuento;
                        }
                    }
                });
        }
    };

    // 1: Paypal  - 3: Contrareembolso
    $scope.paymentChange = function (paymentOption) {
        // Se guarda la forma de pago
        User.pedido.info_pedido_cliente.id_forma_pago = paymentOption;
        // Se lanza el pago con PayPal
        var currencyName;
        if ($scope.selectedCountry.country.code == 'ec' || $scope.selectedCountry.country.code == 'pa') {
            currencyName = "USD";
        } else if ($scope.selectedCountry.country.code == 'es') {
            currencyName = "EUR";
        } else if ($scope.selectedCountry.country.code == 'mx') {
            currencyName = "MXN";
        }
        console.log('currency ' + currencyName);
        if (paymentOption == 1) {
            PaypalService.initPaymentUI().then(function () {
                var total = User.pedido.total_pedido - $scope.cuponDiscount;
                PaypalService.makePayment(total, "Total", $scope.selectedCountry, currencyName)
                    .then(function (data) {
                        // Se comprueba que el pago se haya realizado
                        if (data.response.state && data.response.state == "approved") {
                            // Se guarda el id
                            User.pedido.info_pedido_cliente.id_paypal = data.response.id;
                            $scope.paypalApproved = true;
                            $ionicScrollDelegate.scrollBottom(true);
                            // Se finaliza el pedido automáticamente
                            $scope.submitOrder();
                        }
                    }, function (err) {
                        console.log(err);
                        $scope.paymentChoice.value = 3;
                        User.pedido.info_pedido_cliente.id_forma_pago = 3;
                    });
            });
        }
    };

    $scope.submitOrder = function () {
        $cordovaDialogs.confirm('¿Deseas finalizar tu pedido?', 'Finalizar pedido', ['Finalizar pedido', 'Cancelar'])
            .then(function (buttonIndex) {
                if (buttonIndex == 1) {
                    // Se actualiza su referencia
                    User.pedido.referencia = makeIdReferencia;

                    $ionicLoading.show({
                        template: 'Realizando pedido...'
                    });

                    // Se comprueba si ya hay una fecha introducida, que quiere decir que el restaurante estaba cerrado
                    // y se introdujo el valor en la vista de selección de fecha, lo mantengo. Sino se genera
                    if (!User.pedido.info_pedido_cliente.fecha) {
                        User.pedido.info_pedido_cliente.fecha = $filter('date')(new Date(),'yyyy/MM/dd+HH:mm:ss');
                    }

                    // Se comprueba si hay un descuento aplicado
                    if ($scope.cuponDiscount > 0) {
                        User.pedido.total_pedido = User.pedido.total_pedido - $scope.cuponDiscount;
                        //TODO: resto de descu
                        // Al parecer el server lo quiere con @ en lugar de #
                        User.pedido.descuento = $scope.cuponApplied.replace('#', '@@');
                    }

                    // Se genera el hash de seguridad y se enviará como 'token'
                    console.log('token: ' + User.auth.token);
                    console.log('referencia: '+ User.pedido.referencia);
                    console.log('total_pedido: '+ User.pedido.total_pedido);
                    console.log('fecha: '+ User.pedido.info_pedido_cliente.fecha);

                    var securityToken = md5.createHash(User.auth.token + User.pedido.referencia + User.pedido.total_pedido + User.pedido.info_pedido_cliente.fecha);
                    console.log('hash: ' + securityToken);
                    User.pedido.token = User.auth.token;
                    User.pedido.desechado = securityToken;

                    // Se limpia el pedido de posibles negocios sin productos (porque hayan sido eliminados sus items en
                    // el proceso de pago)
                    User.cleanPedido();
                    console.log("pedido: " + JSON.stringify(User.pedido));
                    // Se realiza el pedido
                    User.setPedido(User.pedido)
                        .then(function (response) {
                            var res = response.data.resultado;
                            if (res) {
                                if (res.code == 0) {
                                    $ionicLoading.show({
                                        template: 'Pedido realizado con éxito'
                                    });
                                    $timeout(function() {
                                        $ionicLoading.hide();
                                        User.clearPedido();
                                        // Limpiar la caché para que no queden restos del pedido realizado
                                        $ionicHistory.clearCache();
                                        $ionicHistory.nextViewOptions({
                                            disableBack: true
                                        });
                                        $state.go('app.order-success');
                                    }, 2000);
                                } else {
                                    // GESTIÓN DE errores
                                    $ionicLoading.hide();
                                }
                            }
                        }, function (error) {
                            console.log(error);
                            $ionicLoading.hide();
                        });
                }
            });
    };

    $scope.goToAddressSelection = function () {
        if (User.auth.isLogged) {
            $state.go('app.order-address');
        } else {
            $ionicViewSwitcher.nextDirection('enter');
            $state.go('login', {
                sourceView: 'cart'
            });
        }
    };

    $scope.decreaseItem = function (restaurantIndex, productIndex) {
        if (User.pedido.pedido[restaurantIndex].productos[productIndex].cantidad == 1) {
            User.pedido.pedido[restaurantIndex].productos.splice(productIndex, 1);
        } else {
            User.pedido.pedido[restaurantIndex].productos[productIndex].cantidad -= 1;
        }

        console.log($scope.totalTransportCost);

        // Se actualiza el coste del transporte por si algún restaurante deja de tener produtos añadidos para no cargar su coste
        User.refreshOrderTransportCost(0);
        console.log($scope.totalTransportCost);
        $scope.totalTransportCost = User.getTotalOrderTransportCost();
        console.log($scope.totalTransportCost);
        //$scope.isTransportCostVariable = User.isTransportCostVariable();
        updateTotalRestaurant(User.pedido.pedido[restaurantIndex].productos, restaurantIndex);
        console.log($scope.totalTransportCost);
    };

    $scope.increaseItem = function (restaurantIndex, productIndex) {
        //User.pedido.pedido[restaurantIndex].productos[productIndex].cantidad += 1;
        // TODO: comprobar este cambio
        User.pedido.pedido[restaurantIndex].productos[productIndex].cantidad = parseInt(User.pedido.pedido[restaurantIndex].productos[productIndex].cantidad) + 1;

        updateTotalRestaurant(User.pedido.pedido[restaurantIndex].productos, restaurantIndex);
    };

    $scope.removeItem = function (restaurantIndex, productIndex) {
        User.pedido.pedido[restaurantIndex].productos.splice(productIndex, 1);

        // Se actualiza el coste del transporte por si algún restaurante deja de tener produtos añadidos para no cargar su coste
        User.refreshOrderTransportCost(0);
        $scope.totalTransportCost = User.getTotalOrderTransportCost();
        //$scope.isTransportCostVariable = User.isTransportCostVariable();
        updateTotalRestaurant(User.pedido.pedido[restaurantIndex].productos, restaurantIndex);
    };

    var updateTotalRestaurant = function (productsInRestaurant, restaurantIndex) {
        var totalImporteProductos = 0;
        // Por cada producto se actualiza el total del importe de este negocio
        for(var i = 0; i < productsInRestaurant.length; i++) {
            totalImporteProductos = totalImporteProductos + (productsInRestaurant[i].importe * productsInRestaurant[i].cantidad);
        }

        // Se actualiza el total de este negocio/restaurante
        User.pedido.pedido[restaurantIndex].total_restaurante = totalImporteProductos +
            User.pedido.pedido[restaurantIndex].transporteFloat;
        // Se actualiza el total del pedido entero
        User.updateTotalPedido();
    };
})

.controller('OrderAddressCtrl', function($scope, $state, User, GlobalsService, $ionicLoading, $cordovaDialogs, $timeout,
                                         ionicDatePicker, $ionicPopup, $filter, getAddressComponentsInfo, NgMap) {
    $scope.$on('$ionicView.afterEnter', function() {
        if (!User.hasAddressesWithFormat('new') && User.hasAddressesWithFormat('old')) {
            $scope.updateAddressesPopup = $ionicPopup.show({
                template: '<div style="text-align: center"> Estamos optimizando el reparto y a partir de ' +
                'ahora todas las direcciones de entrega han de estar <b>geolocalizadas</b>. Actualiza ' +
                'tus direcciones una sola vez y podremos ofrecerte el <b>mejor servicio</b> y tiempo de ' +
                'entregamás rápido posible.</div>',
                title: 'Actualiza tus direcciones',
                scope: $scope,
                buttons: [
                    {
                        text: 'Entendido, actualizar',
                        type: 'button-calm',
                        onTap: function () {
                            // Se actualiza el flag para que no vuelva a mostrar el aviso de por qué
                            // actualizar las direcciones
                            $scope.globalDoc.hideInitialMessage = true;
                            GlobalsService.updateGlobalDoc($scope.globalDoc);

                            // Se abre la ventana modal
                            $scope.updateAddressesModal.show();
                        }
                    }
                ]
            });
        }
/*
        $scope.geoAddresses = angular.copy($scope.globalDoc.user.direcciones);
        removeOldAddresses();
*/
        NgMap.getMap('orderaddresses-map').then(function(map) {
            $scope.orderAddressMap = map;
        }, function (err) {
            console.log('ERROR CARGANDO NMAP');
            console.log(err);
        });
    });

    // Se obtiene el pedido en curso y la BD local
    $scope.pedido = User.pedido;
    $scope.globalDoc = GlobalsService.getGlobalDoc();
    $scope.address = {};
    $scope.selectedAddress = {};
    $scope.isNewAddress = false;
    $scope.isClosedRestaurant = false;
    var disableWeekdays = [];
    var todayDayNumber = (new Date()).getDay();
    var weekDays = ['D', 'L', 'M', 'X', 'J', 'V', 'S'];

    // Se instala un watch para actualizar las direcciones con los cambios
    $scope.$watch('globalDoc.user.direcciones', function() {
        // Se actualiza el selector
        $scope.geoAddresses = angular.copy($scope.globalDoc.user.direcciones);
        removeOldAddresses();
    }, true);

    var geocoder = new google.maps.Geocoder;
    $scope.showMap = false;
    // Variable donde se guardan los datos de la dirección devuelta por el autocomplete de google
    var place = {
        geometry: {
            location: ''
        }
    };

    // Función para filtrar las direcciones antiguas no geolocalizadas, que no aparezcan en el selector
    var removeOldAddresses = function() {
        var i = $scope.geoAddresses.length;
        // Se recorren las direcciones actuales del usuario
        while (i--) {
            // Y se quitan las antiguas
            if ($scope.geoAddresses[i].address == '') {
                $scope.geoAddresses.splice(i, 1);
            }
        }
    };

    $scope.deliveryChoice = {
        value: 1
    };

    $scope.nextAvailableDate = null;
    $scope.selectedDate = null;

    $scope.time = {};
    $scope.time.mytime = new Date();
    $scope.data = {
        value: 1
    };
    $scope.lunchRange = {
        min: null,
        max: null
    };
    $scope.dinnerRange = {
        min: null,
        max: null
    };
    $scope.selectedRange = {
        min: null,
        max: null
    };

    // Si el restaurante está cerrado sólo se deja elegir entre hoy y una fecha dentro de X días
    var limitDate = new Date();
    limitDate.setDate(limitDate.getDate() + 15);

    var timeStrToDate = function (timeStr) {
        return new Date (new Date().toDateString() + ' ' + timeStr);
    };

    var calculateNextAvailableDate = function (listaHorarios) {
        var now = new Date();
        var todayDayLetter = weekDays[todayDayNumber];
        var found = false;
        var iterateDayNumber = todayDayNumber;
        var numberOfDaysAhead = 0;

        // Primero se comprueba si en el día actual existe una próxima hora de apertura
        for (var i = 0; i < listaHorarios[todayDayLetter].length; i++) {
            var restaurantDatetime = timeStrToDate(listaHorarios[todayDayLetter][i].hora_inicio);
            if (now <= restaurantDatetime) {
                found = true;
                // Si todavía no se ha guardado una próxima fecha disponible o la fecha disponible actual es más lejana
                if (!$scope.nextAvailableDate || $scope.nextAvailableDate > restaurantDatetime) {
                    $scope.nextAvailableDate = restaurantDatetime;
                }
                break;
            }
        }

        while(!found) {
            if (iterateDayNumber == 6) {
                iterateDayNumber = 0;
            } else {
                iterateDayNumber += 1;
            }
            numberOfDaysAhead += 1;
            var iterateDayLetter = weekDays[iterateDayNumber];
            // Si está abierto para ese día se obtiene su primer horario
            if (listaHorarios[iterateDayLetter].length > 0) {
                var iterateDate = timeStrToDate(listaHorarios[iterateDayLetter][0].hora_inicio);

                // Le sumo los días que hayan pasado desde la fecha actual
                iterateDate.setDate(iterateDate.getDate() + numberOfDaysAhead);
                found = true;
                // Si todavía no se ha guardado una próxima fecha disponible o la fecha disponible actual es más lejana
                if (!$scope.nextAvailableDate || $scope.nextAvailableDate > iterateDate) {
                    $scope.nextAvailableDate = iterateDate;
                }
            }
        }
    };

    var processClosedRestaurants = function () {
        // Se recorren los restaurantes en busca de cerrados
        for (var i = 0; i < $scope.pedido.pedido.length; i++) {
            if ($scope.pedido.pedido[i].abierto == "0") {
                $scope.isClosedRestaurant = true;

                // Se procesa su lista de horarios
                for (var j = 0; j < weekDays.length; j++) {
                    if ($scope.pedido.pedido[i].lista_horarios[weekDays[j]].length == 0) {
                        // Si no tiene horarios para ese día es que está cerrado, se añade el día a los deshabilitados
                        disableWeekdays.push(j);
                    }
                }

                // Se calcula la próxima fecha en la que esté abierto
                calculateNextAvailableDate($scope.pedido.pedido[i].lista_horarios);
            }
        }
    };
    processClosedRestaurants();

    // Función que recibe la fecha seleccionada y completa los horarios posibles para el primer restaurante que se encuentre
    // abierto en esa fecha
    var calculateTimeRanges = function (selectedDate) {
        var dayNumber = selectedDate.getDay();
        var dayLetter = weekDays[dayNumber];
        // Se comprueba si la fecha seleccionada es el día actual comparando si estamos en el mismo día del mes
        var isToday  = selectedDate.getDate() == (new Date()).getDate();

        // Se recorren los restaurantes
        for (var i = 0; i < $scope.pedido.pedido.length; i++) {
            // Si para ese día el restaurante está abierto
            if ($scope.pedido.pedido[i].lista_horarios[dayLetter].length > 0) {
                // Si la longitud del array es 1 se rellena el horario de comida, si es 2, el de comida y cena
                if ($scope.pedido.pedido[i].lista_horarios[dayLetter].length == 1) {
                    var lunchMinDatetime = timeStrToDate($scope.pedido.pedido[i].lista_horarios[dayLetter][0].hora_inicio);
                    var lunchMaxDatetime = timeStrToDate($scope.pedido.pedido[i].lista_horarios[dayLetter][0].hora_fin);
                    $scope.lunchRange.min = lunchMinDatetime;
                    $scope.lunchRange.max = lunchMaxDatetime;
                    $scope.dinnerRange.min = null;
                    $scope.dinnerRange.max = null;
                    $scope.selectedRange.min = lunchMinDatetime;
                    $scope.selectedRange.max = lunchMaxDatetime;
                    $scope.selectedRange.time = lunchMinDatetime;

                } else if ($scope.pedido.pedido[i].lista_horarios[dayLetter].length == 2) {
                    var lunchMinDatetime = timeStrToDate($scope.pedido.pedido[i].lista_horarios[dayLetter][0].hora_inicio);
                    var lunchMaxDatetime = timeStrToDate($scope.pedido.pedido[i].lista_horarios[dayLetter][0].hora_fin);
                    var dinnerMinDatetime = timeStrToDate($scope.pedido.pedido[i].lista_horarios[dayLetter][1].hora_inicio);
                    var dinnerMaxDatetime = timeStrToDate($scope.pedido.pedido[i].lista_horarios[dayLetter][1].hora_fin);
                    $scope.lunchRange.min = lunchMinDatetime;
                    $scope.lunchRange.max = lunchMaxDatetime;
                    $scope.dinnerRange.min = dinnerMinDatetime;
                    $scope.dinnerRange.max = dinnerMaxDatetime;
                    $scope.selectedRange.time = lunchMinDatetime;
                }
                break;
            }
        }
    };

    var ipObj1 = {
        callback: function (val) {  //Mandatory
            $scope.selectedDate = new Date(val);
            // Se guarda la fecha seleccionada del calendario
            ipObj1.inputDate = new Date(val);
            // Se calculan los horarios posibles para el día seleccionado, se le envía el nº del día de la semana
            calculateTimeRanges($scope.selectedDate);

        },
        from: new Date(),
        to: limitDate,
        mondayFirst: true,          //Optional
        disableWeekdays: disableWeekdays,
        closeOnSelect: false,       //Optional
        templateType: 'popup'       //Optional
    };

    $scope.openDatePicker = function(){
        ionicDatePicker.openDatePicker(ipObj1);
    };

    $scope.showPopup = function() {
        var myPopup = $ionicPopup.show({
            templateUrl: 'templates/delivery-time-popup.html',
            title: 'Selecciona la hora',
            scope: $scope,
            buttons: [
                {
                    text: '<b>Guardar</b>',
                    type: 'button-calm',
                    onTap: function (e) {
                        // Se comprueba que la hora esté dentro de los márgenes establecidos
                        if ($scope.data.value == 1) {   // comida seleccionada
                            if ($scope.selectedRange.time < $scope.lunchRange.min || $scope.selectedRange.time > $scope.lunchRange.max) {
                                e.preventDefault();
                            }
                        } else if ($scope.data.value == 2) {   //cena seleccionada
                            if ($scope.selectedRange.time < $scope.dinnerRange.min || $scope.selectedRange.time > $scope.dinnerRange.max) {
                                e.preventDefault();
                            }
                        }
                    }
                }
            ]
        });
    };

    $scope.mealTimeChange = function (value) {
        // 1 - comida . 2 - cena
        if (value == 1) {
            $scope.selectedRange.min = $scope.lunchRange.min;
            $scope.selectedRange.max = $scope.lunchRange.max;
            $scope.selectedRange.time = $scope.lunchRange.min;
        } else if (value == 2) {
            $scope.selectedRange.min = $scope.dinnerRange.min;
            $scope.selectedRange.max = $scope.dinnerRange.max;
            $scope.selectedRange.time = $scope.dinnerRange.min;
        }
    };

    // Si existe una dirección actual, se rellena el formulario con esos datos
    if ($scope.globalDoc.currentAddress) {
        $scope.selectedAddress.address = $scope.globalDoc.currentAddress;

        // Se actualiza la información del pedido
        User.pedido.info_pedido_cliente.id_ciudad = $scope.selectedAddress.address.id_city;
        User.pedido.info_pedido_cliente.id_direccion = $scope.selectedAddress.address.id_direccion;
    }

    $scope.goToCartSummary = function () {
        // Se comprueba que se reparta en la dirección seleccionada
        var checkAddressObj = {};
        checkAddressObj.lat = $scope.selectedAddress.address.lat;
        checkAddressObj.lng = $scope.selectedAddress.address.lng;

        User.checkAddress(checkAddressObj)
            .then(function () {
                console.log('checkaddres gotocart bien');

                // Se actualiza la dirección actual, se borra la propiedad que mete angular: el haskKey
                if ($scope.selectedAddress.address.$$hashKey) {
                    delete $scope.selectedAddress.address.$$hashKey;
                }
                $scope.globalDoc.currentAddress = angular.copy($scope.selectedAddress.address);
                GlobalsService.updateGlobalDoc($scope.globalDoc);

                // Si había algún restaurante cerrado, se guarda la fecha de reparto
                if ($scope.isClosedRestaurant) {
                    // Se comprueba si elige 'lo antes posible' o una fecha concreta
                    if ($scope.deliveryChoice.value == 1) {  // lo antes posible
                        User.pedido.info_pedido_cliente.fecha = $filter('date')($scope.nextAvailableDate,'yyyy/MM/dd+HH:mm:ss');
                    } else if ($scope.deliveryChoice.value == 2) { // fecha concreta
                        // Primero se coge la fecha seleccionada
                        var deliveryDate = $scope.selectedDate;
                        // Y luego se modifica su hora, con el horario seleccionado
                        deliveryDate.setHours($scope.selectedRange.time.getHours());
                        deliveryDate.setMinutes($scope.selectedRange.time.getMinutes());
                        deliveryDate.setMilliseconds($scope.selectedRange.time.getMilliseconds());
                        User.pedido.info_pedido_cliente.fecha = $filter('date')(deliveryDate,'yyyy/MM/dd+HH:mm:ss');
                    }
                }

                $state.go('app.summary');

            }, function () {
                console.log('checkaddres gotocart mal');
            });
    };

    $scope.addressChange = function (address, oldAddress) {
        console.log('addres 10 : ' + address);
        console.log(oldAddress);
        $scope.isNewAddress = false;
        $scope.showMap = false;

        // Se actualiza la información del pedido
        //User.pedido.info_pedido_cliente.id_ciudad = address.id_ciudad;
        if (address) {
            var checkAddressObj = {};
            checkAddressObj.lat = address.lat;
            checkAddressObj.lng = address.lng;

            User.checkAddress(checkAddressObj)
                .then(function () {
                    console.log('checkaddres bien');
                    User.pedido.info_pedido_cliente.id_ciudad = address.id_city;
                    User.pedido.info_pedido_cliente.id_direccion = address.id_direccion;
                }, function () {
                    console.log('checkaddres mal');
                    $scope.selectedAddress.address = JSON.parse(oldAddress);
                });
        }
    };

    $scope.newAddress = function () {
        $scope.isNewAddress = true;
        $scope.selectedAddress.address = {};
        // Se actualiza la información del pedido
        User.pedido.info_pedido_cliente.id_ciudad = "";
        User.pedido.info_pedido_cliente.id_direccion = "";

        // Rellenamos los campos con la dirección de búsqueda
        if ($scope.globalDoc.searchAddress) {
            $scope.address.address =  $scope.globalDoc.searchAddress.Street;
            $scope.address.lat = $scope.globalDoc.searchAddress.Latitude;
            $scope.address.lng = $scope.globalDoc.searchAddress.Longitude;

            window.setTimeout(function(){
                google.maps.event.trigger($scope.orderAddressMap, 'resize'); // resize the map with a short delay
                $scope.orderAddressMap.setCenter(
                    new google.maps.LatLng($scope.address.lat, $scope.address.lng)
                );
            },100);

            $scope.showMap = true;
        }
    };

    $scope.saveAddress = function () {
        $ionicLoading.show({
            template: 'Guardando...'
        });

        // Se le añade al objeto la acción realizada
        $scope.address.accion = 1;
        // Si es una nueva dirección no existe el id_direccion actual
        if ($scope.isNewAddress) {
            $scope.address.id_direccion = "-1";
        }
        console.log('direccion : ' + JSON.stringify($scope.address));
        User.setDireccion($scope.address)
            .then(function (response) {
                $ionicLoading.hide();
                var res = response.data.resultado;
                if (res) {
                    // Si el usuario no existe (-1), o hay error en el token (-5), se deslogea al usuario
                    if (res.code == -1 || res.code == -5) {
                        $ionicLoading.show({
                            template: 'Sesión iniciada en otro dispositivo: reiniciando..'
                        });
                        User.refreshData();
                        $timeout(function() {
                            $ionicLoading.hide();
                        }, 2000);
                    } else if (res.code == -4) { //Error en la llamada. No se incluyen datos necesarios
                        $cordovaDialogs.alert('Datos incompletos', 'Guardar dirección', 'OK');
                    } else if (res.code == -3) { // Dirección fuera de reparto
                        $cordovaDialogs.alert('Lo sentimos, pero de momento no repartimos en esta zona',
                            'Dirección fuera de reparto', 'OK');
                    } else if (res.code == 0) {
                        // Se limpia la dirección
                        delete $scope.address.accion;
                        delete $scope.address.$$hashKey;
                        delete $scope.address.token;

                        $scope.address.id_city = res.id_ciudad;

                        // Si es una nueva dirección se guarda la dirección, si no se actualiza
                        if ($scope.isNewAddress) {
                            $scope.address.id_direccion = res.id.toString();
                            $scope.globalDoc.user.direcciones.push($scope.address);
                            $scope.isNewAddress = false;
                        } else {
                            // Se busca el objeto con el id_direccion en el array de direcciones del usuario
                            for (var i = 0; i < $scope.globalDoc.user.direcciones.length; i++) {
                                if ($scope.globalDoc.user.direcciones[i].id_direccion == res.id.toString()) {
                                    $scope.globalDoc.user.direcciones[i] = $scope.address;
                                    GlobalsService.updateGlobalDoc($scope.globalDoc);
                                    break;
                                }
                            }
                        }
                        // Se actualiza la información del pedido
                        //User.pedido.info_pedido_cliente.id_ciudad = $scope.address.id_ciudad;

                        $ionicLoading.show({
                            template: 'Dirección guardada con éxito!'
                        });
                        $timeout(function() {
                            $ionicLoading.hide();

                            var checkAddressObj = {};
                            checkAddressObj.lat = $scope.address.lat;
                            checkAddressObj.lng = $scope.address.lng;

                            User.checkAddress(checkAddressObj)
                                .then(function () {
                                    console.log('checkaddres bien');
                                    User.pedido.info_pedido_cliente.id_ciudad = $scope.address.id_city;
                                    User.pedido.info_pedido_cliente.id_direccion = $scope.address.id_direccion;

                                    // TODO: comprobar si reparte y nuevo transporte

                                    // Se actualiza la dirección actual
                                    if ($scope.address.$$hashKey) {
                                        delete $scope.address.$$hashKey;
                                    }
                                    $scope.globalDoc.currentAddress = $scope.address;
                                    $scope.selectedAddress.address = $scope.address;
                                    GlobalsService.updateGlobalDoc($scope.globalDoc);
                                }, function () {
                                    console.log('checkaddres mal');
                                });
                        }, 2000);
                    }
                }
            }, function () {
                console.log('SET DIRECCION KO');
                $ionicLoading.hide();
                $cordovaDialogs.alert('Servicio temporalmente no disponible', 'Guardar dirección', 'OK');
            });
    };

    $scope.orderAddressesPlaceChanged = function() {
        $scope.showMap = true;
        placeObtained = this.getPlace();

        window.setTimeout(function(){
            google.maps.event.trigger($scope.orderAddressMap, 'resize'); // resize the map with a short delay
            $scope.orderAddressMap.setZoom(17);
            $scope.orderAddressMap.setCenter(placeObtained.geometry.location);
        },100);

        // Se actualiza la información de la dirección
        updateOldAddressObject(placeObtained);
    };

    $scope.orderAddressesGetMarkerLocation = function () {
        // Se obtienen las coordenadas del marker que se ha desplazado
        place.geometry.location = this.getPosition();
        // Se centra el mapa en dicho punto
        $scope.orderAddressMap.setCenter(place.geometry.location);

        // Se llama al servicio de geocodificación inversa, para obtener los datos de la dirección a partir de coordenadas
        geocoder.geocode({'location': place.geometry.location}, function(results, status) {
            if (status === google.maps.GeocoderStatus.OK) {
                if (results[0]) {
                    $scope.address.address = results[0].formatted_address;
                    $scope.address.lat = place.geometry.location.lat();
                    $scope.address.lng = place.geometry.location.lng();
                    place.formatted_address = results[0].formatted_address;

                    // Se actualiza la información de la dirección
                    updateOldAddressObject(results[0]);
                    // Se fuerza a actualizar el scope ya que hay algún problema por el cual no actualiza el digest cycle
                    $scope.$apply();
                } else {
                    console.log('No results found');
                }
            } else {
                console.log('Geocoder failed due to: ' + status);
            }
        });
    };

    var updateOldAddressObject = function (placeObtained) {
        $scope.address.lat = placeObtained.geometry.location.lat();
        $scope.address.lng = placeObtained.geometry.location.lng();

        angular.extend($scope.address, getAddressComponentsInfo(placeObtained.address_components));
    };
})

.controller('OrderSuccessCtrl', function($scope, $ionicHistory, $state, User, GlobalsService) {
    $scope.globalDoc = GlobalsService.getGlobalDoc();
    // Se refrescan los datos de usuario para actualizar los pedidos
    User.refreshData();

    $scope.newSearch = function () {
        $ionicHistory.nextViewOptions({
            historyRoot: true
        });

        $state.go('app.find-rest');
    };
})

.controller('ProfileCtrl', function($scope, GlobalsService, $ionicHistory, $state, User, $ionicModal, md5, $timeout,
                                    $cordovaDialogs, $ionicLoading, Business) {
    $scope.globalDoc = GlobalsService.getGlobalDoc();
    $scope.countries = User.countries;

    $ionicModal.fromTemplateUrl('templates/personal-info.html', {
        scope: $scope,
        animation: 'slide-in-right'
    }).then(function(modal) {
        $scope.personalInfoModal = modal;
    });

    $scope.goHome = function () {
        $ionicHistory.nextViewOptions({
            historyRoot: true
        });
        $state.go('app.find-rest');
    };

    $scope.showPersonalInfo = function () {
        // Se copia la info necesario para actualizar el usuario
        $scope.user = {};
        $scope.user.nombre = $scope.globalDoc.user.nombre;
        $scope.user.apellidos = $scope.globalDoc.user.apellidos;
        $scope.user.username = $scope.globalDoc.user.email;
        $scope.user.movil = $scope.globalDoc.user.movil;
        $scope.user.telefono = $scope.globalDoc.user.telefono;
        // El toggle es como un checkbox y necesita de variables booleanos, por lo que hay que convertirlos
        $scope.user.newsletterBool = $scope.globalDoc.user.newsletter == '1' ? true : false;
        $scope.user.ofertasBool = $scope.globalDoc.user.ofertas == '1' ? true : false;

        // Se abre la ventana modal
        $scope.personalInfoModal.show();
    };

    $scope.updatePersonalInfo = function () {
        // TODO: En producción parece que no es necesario enviar apellidos ni telefono
        // Se calcula el md5 de la contraseña para enviar al servidor
        $scope.user.password = md5.createHash($scope.user.password);
        // Se procesan los 'booleanos' para enviarlos al servidor según su manera de cogerlos..
        $scope.user.newsletter = $scope.user.newsletterBool == true ? '1' : '0';
        $scope.user.ofertas = $scope.user.ofertasBool == true ? '1' : '0';
        console.log('usuario : ' + JSON.stringify($scope.user));
        User.updateUser($scope.user)
            .then(function(response) {
                var res = response.data.resultado;
                if (res) {
                    if (res.code == -4) {
                        $cordovaDialogs.alert('Datos incorrectos', 'Actualizar usuario', 'OK');
                    } else if (res.code == -2) {
                        $cordovaDialogs.alert('Ya existe un usuario con ese correo', 'Actualizar usuario', 'OK');
                    } else if (res.code == 0) {
                        // Se actualiza el usuario persistido en local con los datos enviados al servidor
                        $scope.globalDoc.user.nombre = $scope.user.nombre;
                        $scope.globalDoc.user.apellidos = $scope.user.apellidos;
                        $scope.globalDoc.user.email = $scope.user.username;
                        $scope.globalDoc.user.movil = $scope.user.movil;
                        $scope.globalDoc.user.telefono = $scope.user.telefono;
                        $scope.globalDoc.user.newsletter = $scope.user.newsletter;
                        $scope.globalDoc.user.ofertas = $scope.user.ofertas;
                        GlobalsService.updateGlobalDoc($scope.globalDoc);

                        $ionicLoading.show({
                            template: 'Datos actualizados'
                        });
                        $timeout(function() {
                            $ionicLoading.hide();
                            // Se cierra la ventana modal
                            $scope.personalInfoModal.hide();
                        }, 3000);
                    } else if (res.code == -3) {
                        // Token incorrecto
                        $ionicLoading.show({
                            template: 'Reiniciando sesión'
                        });
                        // Se reinicia la sesión del usuario
                        User.refreshData();

                        $timeout(function() {
                            $ionicLoading.hide();
                            //$scope.updatePersonalInfo();
                        }, 3000);
                    } else {
                        $cordovaDialogs.alert('Error actualizando datos de usuario', 'Actualizar usuario', 'OK');
                    }
                }
            }, function () {
                console.log('UPDATE USER KO');
                $cordovaDialogs.alert('Servicio temporalmente no disponible', 'Actualizar usuario', 'OK');
            });

    };

    $scope.logout = function() {
        // Se utiliza el servicio para hacer logout al usuario
        User.logout();
    };
})

.controller('MyOrdersCtrl', function($scope, $ionicHistory, $state, pedidos, User, Business, $ionicLoading, $cordovaDialogs) {
    // Se obtienen los pedidos de la BD local
    $scope.pedidos = pedidos;

    User.clearPedido();

    $scope.isBackView = function () {
        return $ionicHistory.backView() ? true : false;
    };

    // Si se recibe 'home' se va a la 'home', en caso contrario se vuelve hacia atrás
    $scope.goBackwards = function (target) {
        if (target == 'home') {
            $ionicHistory.nextViewOptions({
                historyRoot: true
            });
            $state.go('app.find-rest');
        } else {
            $ionicHistory.goBack();
        }
    };

    $scope.orderByDate = function(item) {
        var parts = item.fecha_registro.split('/');
        var date = new Date(parseInt(parts[2]),
            parseInt(parts[1]),
            parseInt(parts[0]));

        return date;
    };

    $scope.reuseOrderAndGoToCart = function (order) {
        console.log(order);

        var pedidoObj = {};
        pedidoObj.pedido = order.id_pedido;

        $ionicLoading.show({
            template: 'Cargando...'
        });
        Business.getPedido(pedidoObj)
            .then(function(response) {
                var res = response.data.resultado;
                if (res) {
                    if (res.code == 0) {
                        $ionicLoading.hide();
                        console.log(res);
                        angular.extend(User.pedido, res.pedido);

                        // Se cambia la dirección actual por la que tenga en ese pedido
                        if (!User.changeCurrentAddress(User.pedido.info_pedido_cliente.id_direccion)) {
                            // TODO: se hace algo? se le indica que no tiene esa dirección ya y no se le deja?
                        }

                        // Se procesa para que tenga los campos correcto y en la forma que se necesita en el carrito
                        User.pedido.info_pedido_cliente.id_descuentos_globales = "";
                        User.pedido.info_pedido_cliente.id_forma_pago = 3;
                        User.pedido.info_pedido_cliente.id_descuento_cliente = 3;
                        User.pedido.info_pedido_cliente.id_paypal = "";

                        // Se recorren los pedidos
                        for(var i = 0; i < User.pedido.pedido.length; i++) {
                            if (User.pedido.pedido[i].transport == -1) {
                                $cordovaDialogs.alert('El negocio ya no reparte en esa dirección', 'Reutilizar pedido', 'OK');

                                return;
                            }
                            User.pedido.pedido[i].transporte = User.pedido.pedido[i].transport;
                            User.pedido.pedido[i].transporteFloat = parseFloat(User.pedido.pedido[i].transporte);
                        }

                        $state.go('app.cart');
                    } else {
                        $ionicLoading.hide();
                        $cordovaDialogs.alert('Servicio temporalmente no disponible', 'Reutilizar pedido', 'OK');
                    }
                } else {
                    $ionicLoading.hide();
                    $cordovaDialogs.alert('Servicio temporalmente no disponible', 'Reutilizar pedido', 'OK');
                }

            }, function () {
                $ionicLoading.hide();
                $cordovaDialogs.alert('Servicio temporalmente no disponible', 'Reutilizar pedido', 'OK');
            });
    };
})

.controller('MahalosCtrl', function($scope, $ionicHistory, $state, pedidos, Business, $cordovaDialogs, $ionicLoading,
                                    GlobalsService, User, $timeout) {
    // Se obtienen los pedidos de la BD local
    $scope.pedidos = pedidos;
    $scope.totalMahalos = 0;
    var globalDoc = GlobalsService.getGlobalDoc();

    // Función que calcula el total de los puntos Mahalos
    var calculateTotalMahalos = function() {
        if ($scope.pedidos.info) {
            for(var i = 0; i < $scope.pedidos.info.length; i++) {
                var mahalos = parseFloat($scope.pedidos.info[i].mahalos);
                if (mahalos > 0) {
                    $scope.totalMahalos = $scope.totalMahalos + mahalos;
                }
            }
            // Se redondea el total
            $scope.totalMahalos = Math.round( $scope.totalMahalos * 1e2 ) / 1e2;
        }
    };
    calculateTotalMahalos();

    $scope.areMahalos = function() {
        return function( item ) {
            return parseFloat(item.mahalos) > 0;
        };
    };

    $scope.setMahalos = function () {
        // Se muestra un 'cargando..' mientras se canjean
        $ionicLoading.show({
            template: 'Canjeando Mahalos...'
        });
        Business.setMahalos()
            .then(function(response) {
                var res = response.data.resultado;
                if (res) {
                    if (res.code == 0) {
                        // Si todo va bien hay que actualizar la info del usuario, ahora mismo sólo se puede hacer con una
                        // llamada al 'login' o poniendo a false 'hay_mahalos' y actualizando sólo los descuentos
                        globalDoc.user.pedidos.hay_mahalos = false;
                        $scope.pedidos.hay_mahalos = false;
                        // Se actualizan los descuentos
                        // TODO: controlar posibles errores y deslogear al usuario en tal caso
                        Business.getDescuentos()
                            .then(function(response) {
                                $ionicLoading.hide();
                                var res = response.data.resultado;
                                if (res) {
                                    if (res.code == 0) {
                                        globalDoc.user.descuentos = res.descuentos;
                                        GlobalsService.updateGlobalDoc(globalDoc);
                                        $ionicLoading.show({
                                            template: 'Se ha generado el cupón de descuento'
                                        });
                                        $timeout(function() {
                                            $ionicLoading.hide();
                                            $ionicHistory.goBack();
                                        }, 2000);
                                    }
                                }
                            }, function () {
                                // TODO: si algo va mal deslogear al usuario
                                $ionicLoading.hide();
                            });
                    } else {
                        $ionicLoading.show({
                            template: 'Es necesario que vuelvas a iniciar sesión'
                        });
                        $timeout(function() {
                            $ionicLoading.hide();
                            User.logout();
                        }, 2000);
                    }
                } else {
                    $ionicLoading.hide();
                }

            }, function () {
                $ionicLoading.hide();
                console.log('set mahalos KO');
                $cordovaDialogs.alert('Servicio temporalmente no disponible', 'Canjear Mahalos', 'OK');
            });
    };
})

.controller('ContactCtrl', function($scope, contact, $ionicHistory, $state) {
    $scope.contact = contact;

    $scope.goHome = function () {
        $ionicHistory.nextViewOptions({
            historyRoot: true
        });
        $state.go('app.find-rest');
    };
})

.controller('DiscountsCtrl', function($scope, descuentos) {
    $scope.descuentos = descuentos;
})

.controller('MyAddressesCtrl', function($scope, GlobalsService, User, $cordovaDialogs, $ionicLoading, $timeout, $filter,
                                        $ionicHistory, getAddressComponentsInfo, NgMap) {
    var geocoder = new google.maps.Geocoder;
    var clearMap = function () {
        $scope.addressMap.setCenter(new google.maps.LatLng(40.420146328111386, -5.7038846846558044));
        $scope.addressMap.setZoom(4);
    };

    $scope.address = {};
    $scope.selectedAddress = {};
    $scope.isNewAddress = false;
    $scope.showMap = true;

    // Variable donde se guardan los datos de la dirección devuelta por el autocomplete de google
    var place = {
        geometry: {
            location: ''
        }
    };

    // Se obtienen los datos guardados en local y se procesan las direcciones para eliminar las antiguas
    $scope.globalDoc = GlobalsService.getGlobalDoc();
    $scope.geoAddresses = angular.copy($scope.globalDoc.user.direcciones);

    // Función para filtrar de las direcciones las que son antiguas y dejar el selector sólo con nuevas (geolocalizadas)
    var removeOldAddresses = function() {
        var i = $scope.geoAddresses.length;
        // Se recorren las direcciones actuales del usuario
        while (i--) {
            // Y se quitan las antiguas
            if ($scope.geoAddresses[i].address == '') {
                $scope.geoAddresses.splice(i, 1);
            }
        }
    };
    removeOldAddresses();

    NgMap.getMap('myaddresses-map').then(function(map) {
        $scope.addressMap = map;
        // Si existe una dirección actual, se situa el mapa en la dirección
        if ($scope.globalDoc.currentAddress && !angular.equals($scope.globalDoc.currentAddress, {})) {
            $scope.addressMap.setZoom(17);
            $scope.addressMap.setCenter(
                new google.maps.LatLng($scope.globalDoc.currentAddress.lat, $scope.globalDoc.currentAddress.lng)
            );
        } else {
            clearMap();
        }
    }, function (err) {
        console.log(err);
    });

    // Función que comprueba si el usuario tiene pendientes direcciones para actualizar (geolocalizar)
    $scope.hasOldAddresses = function () {
        return User.hasAddressesWithFormat('old');
    };

    // Si existe una dirección actual, se limpia y se rellena el formulario con esos datos
    if ($scope.globalDoc.currentAddress && !angular.equals($scope.globalDoc.currentAddress, {})) {
        $scope.address = angular.copy($scope.globalDoc.currentAddress);
        $scope.selectedAddress.address = $scope.globalDoc.currentAddress;
    } else {
        $scope.isNewAddress = true;
        $scope.showMap = false;
    }

    $scope.addressChange = function (address) {
        console.log('addres 5 : ' + address);
        // Se comprueba que venga el parámetro address, es decir, que no haya clicado en 'Selecciona una dirección'
        if (address) {
            $scope.isNewAddress = false;
            $scope.address = angular.copy(address);

            // Se muestra el mapa con la dirección seleccionada
            $scope.showMap = true;

            window.setTimeout(function(){
                google.maps.event.trigger($scope.addressMap, 'resize'); // resize the map with a short delay
                $scope.addressMap.setZoom(17);
                $scope.addressMap.setCenter(
                    new google.maps.LatLng(address.lat, address.lng)
                );
            },100);

            // Se actualiza la dirección actual
            $scope.globalDoc.currentAddress = address;
            GlobalsService.updateGlobalDoc($scope.globalDoc);
        } else {
            $scope.newAddress();
        }
    };

    $scope.newAddress = function () {
        $scope.isNewAddress = true;
        $scope.showMap = false;
        $scope.address = {};
        $scope.selectedAddress.address = {};
        clearMap();
    };

    var updateOldAddressObject = function (placeObtained) {
        $scope.address.lat = placeObtained.geometry.location.lat();
        $scope.address.lng = placeObtained.geometry.location.lng();

        angular.extend($scope.address, getAddressComponentsInfo(placeObtained.address_components));
    };

    $scope.myAddressesPlaceChanged = function() {
        $scope.showMap = true;
        placeObtained = this.getPlace();

        window.setTimeout(function(){
            google.maps.event.trigger($scope.addressMap, 'resize'); // resize the map with a short delay
            $scope.addressMap.setZoom(17);
            $scope.addressMap.setCenter(placeObtained.geometry.location);
        },100);

        // Se actualiza la información de la dirección
        updateOldAddressObject(placeObtained);
    };

    $scope.show = function () {
        console.log($scope.globalDoc.user.direcciones);
        console.log($scope.globalDoc.currentAddress);
    };

    $scope.myAddressesGetMarkerLocation = function () {
        // Se obtienen las coordenadas del marker que se ha desplazado
        place.geometry.location = this.getPosition();
        // Se centra el mapa en dicho punto
        $scope.addressMap.setCenter(place.geometry.location);

        // Se llama al servicio de geocodificación inversa, para obtener los datos de la dirección a partir de coordenadas
        geocoder.geocode({'location': place.geometry.location}, function(results, status) {
            if (status === google.maps.GeocoderStatus.OK) {
                if (results[0]) {
                    $scope.address.address = results[0].formatted_address;
                    $scope.address.lat = place.geometry.location.lat();
                    $scope.address.lng = place.geometry.location.lng();
                    place.formatted_address = results[0].formatted_address;

                    // Se actualiza la información de la dirección
                    updateOldAddressObject(results[0]);

                    // Se fuerza a actualizar el scope ya que hay algún problema por el cual no actualiza el digest cycle
                    $scope.$apply();
                } else {
                    console.log('No results found');
                }
            } else {
                console.log('Geocoder failed due to: ' + status);
            }
        });
    };

    $scope.removeAddress = function () {
        // Sólo se muestra la confirmación si hay una dirección seleccionada para eliminar
        if ($scope.address.id_direccion) {
            var removeMessage = "¿Estás seguro que quieres eliminar la dirección: '" + $scope.address.address + "'?";
            $cordovaDialogs.confirm(removeMessage, 'Eliminar dirección', ['Eliminar', 'Cancelar'])
                .then(function(buttonIndex) {
                    // Si escoge eliminar
                    if (buttonIndex == 1) {
                        $ionicLoading.show({
                            template: 'Eliminando dirección...'
                        });

                        // Se le añade al objeto la acción realizada
                        $scope.address.accion = 2;

                        User.setDireccion($scope.address)
                            .then(function (response) {
                                $ionicLoading.hide();
                                var res = response.data.resultado;
                                if (res) {
                                    // Si el usuario no existe (-1), o hay error en el token (-5), se deslogea al usuario
                                    if (res.code == -1 || res.code == -5) {
                                        $ionicLoading.show({
                                            template: 'Sesión iniciada en otro dispositivo: reiniciando..'
                                        });
                                        User.refreshData();
                                        $timeout(function() {
                                            $ionicLoading.hide();
                                        }, 2000);
                                    } else if (res.code == -4) { //Error en la llamada. No se incluyen datos necesarios
                                        $cordovaDialogs.alert('Datos incompletos', 'Eliminar dirección', 'OK');
                                    } else if (res.code == 0) {
                                        // TODO: borrar dirección
                                        // Se limpia la dirección seleccionada
                                        $scope.selectedAddress.address = {};
                                        var idDireccion = $scope.address.id_direccion;
                                        $scope.address = {};
                                        delete $scope.globalDoc.currentAddress;

                                        // Se busca el objeto con el id_direccion en el array de direcciones del usuario
                                        // para borrarlo
                                        for (var i = 0; i < $scope.globalDoc.user.direcciones.length; i++) {
                                            if ($scope.globalDoc.user.direcciones[i].id_direccion == idDireccion.toString()) {
                                                $scope.globalDoc.user.direcciones.splice(i, 1);
                                                GlobalsService.updateGlobalDoc($scope.globalDoc);
                                                break;
                                            }
                                        }

                                        // Se coge como dirección actual la primera que haya
                                        if ($scope.globalDoc.user.direcciones.length) {
                                            $scope.selectedAddress.address = $scope.globalDoc.user.direcciones[0];
                                            $scope.globalDoc.currentAddress = $scope.globalDoc.user.direcciones[0];
                                            GlobalsService.updateGlobalDoc($scope.globalDoc);
                                        }
                                        $ionicLoading.show({
                                            template: 'Dirección eliminada con éxito!'
                                        });
                                        $timeout(function() {
                                            $scope.address = {};
                                            $ionicLoading.hide();
                                            $ionicHistory.goBack();
                                        }, 2000);
                                    }
                                }
                            }, function () {
                                console.log('SET DIRECCION KO');
                                $ionicLoading.hide();
                                $cordovaDialogs.alert('Servicio temporalmente no disponible', 'Eliminar dirección', 'OK');
                            });
                    }

                });
        }

    };

    $scope.saveAddress = function () {
        $ionicLoading.show({
            template: 'Guardando...'
        });

        // Se le añade al objeto la acción realizada
        $scope.address.accion = 1;
        // Si es una nueva dirección no existe el id_direccion actual
        if ($scope.isNewAddress) {
            $scope.address.id_direccion = "-1";
        }
        console.log('direccion 99 ' + JSON.stringify($scope.address));
        User.setDireccion($scope.address)
            .then(function (response) {
                console.log('direccion 100 ' + JSON.stringify($scope.address));
                $ionicLoading.hide();
                var res = response.data.resultado;
                if (res) {
                    // Si el usuario no existe (-1), o hay error en el token (-5), se deslogea al usuario
                    if (res.code == -1 || res.code == -5) {
                        $ionicLoading.show({
                            template: 'Sesión iniciada en otro dispositivo: reiniciando..'
                        });
                        User.refreshData();
                        $timeout(function() {
                            $ionicLoading.hide();
                        }, 2000);
                    } else if (res.code == -4) { //Error en la llamada. No se incluyen datos necesarios
                        $cordovaDialogs.alert('Datos incompletos', 'Guardar dirección', 'OK');
                    } else if (res.code == -3) { // Dirección fuera de reparto
                        $cordovaDialogs.alert('Lo sentimos, pero de momento no repartimos en esta zona',
                            'Dirección fuera de reparto', 'OK');
                    } else if (res.code == 0) {
                        // Se limpia la dirección
                        delete $scope.address.accion;
                        delete $scope.address.$$hashKey;
                        delete $scope.address.token;

                        // Si es una nueva dirección se guarda la dirección, si no se actualiza
                        if ($scope.isNewAddress) {
                            // TODO : OJO! buscar este cambio de ws en toda la app
                            $scope.address.id_direccion = res.id.toString();
                            //$scope.address.id_direccion = res.id.resultado.toString();

                            // Se actualiza la ciudad con el id devuelto
                            $scope.address.id_city = res.id_ciudad;
                            $scope.globalDoc.user.direcciones.push($scope.address);
                            $scope.isNewAddress = false;
                        } else {
                            // Se busca el objeto con el id_direccion en el array de direcciones del usuario
                            for (var i = 0; i < $scope.globalDoc.user.direcciones.length; i++) {
                                if ($scope.globalDoc.user.direcciones[i].id_direccion == res.id.toString()) {
                                    $scope.globalDoc.user.direcciones[i] = $scope.address;
                                    // Se actualiza la ciudad con el id devuelto
                                    $scope.globalDoc.user.direcciones[i].id_city = res.id_ciudad;
                                    GlobalsService.updateGlobalDoc($scope.globalDoc);
                                    break;
                                }
                            }
                        }

                        // Se actualiza la dirección actual
                        if ($scope.address.$$hashKey) {
                            delete $scope.address.$$hashKey;
                        }
                        $scope.globalDoc.currentAddress = $scope.address;
                        $scope.selectedAddress.address = $scope.address;
                        GlobalsService.updateGlobalDoc($scope.globalDoc);

                        // Se actualiza el selector
                        $scope.geoAddresses = angular.copy($scope.globalDoc.user.direcciones);
                        removeOldAddresses();

                        $ionicLoading.show({
                            template: 'Dirección guardada con éxito!'
                        });
                        $timeout(function() {
                            $ionicLoading.hide();
                        }, 2000);
                    }
                }
            }, function () {
                console.log('SET DIRECCION KO');
                $ionicLoading.hide();
                $cordovaDialogs.alert('Servicio temporalmente no disponible', 'Guardar dirección', 'OK');
            });
    };
})

.controller('FavouritesCtrl', function($scope, favoritos, $ionicLoading, $cordovaDialogs, Business, $state, User,
                                       GlobalsService) {
    $scope.favoritos = favoritos;
    $scope.globalDoc = GlobalsService.getGlobalDoc();

    $scope.removeFavourite = function (favorito) {
        $cordovaDialogs.confirm("¿Quieres quitar de favorito '" + favorito.nombre +"'?",
            'Eliminar favorito', ['Sí, quitar', 'No, cancelar'])
            .then(function(buttonIndex) {
                // Si escoge Sí, quitar
                if (buttonIndex == 1) {
                    var restaurantObj = {};
                    restaurantObj.id_negocio = favorito.id_negocio;

                    User.setFavorito(restaurantObj)
                        .then(function (response) {
                            var res = response.data.resultado;
                            if (res) {
                                if (res.code == 1 || res.code == 2) {
                                    // Se actualizan los favoritos
                                    User.getFavoritos()
                                        .then(function (response) {
                                            var res = response.data.resultado;
                                            if (res.code == 0) {
                                                $scope.globalDoc.user.favoritos = res.favoritos;
                                                $scope.favoritos = res.favoritos;
                                                GlobalsService.updateGlobalDoc($scope.globalDoc);
                                            }
                                        });
                                }
                            }
                        });
                }
            });
    };

    $scope.goToBusiness = function(business) {
        // Se crea el objeto para rellenar con parámetros del ws a enviar
        var businessObject = {};

        console.log($scope.globalDoc.currentAddress);

        // Se comprueba si el usuario ya tiene seleccionada una dirección
        if (!$scope.globalDoc.currentAddress || angular.equals({}, $scope.globalDoc.currentAddress)) {
            console.log('no tiene currentAddress');
            // Si no tiene, se coge como dirección actual la primera que haya
            if ($scope.globalDoc.user.direcciones.length) {
                $scope.globalDoc.currentAddress = $scope.globalDoc.user.direcciones[0];
            }
        }

        // Se completa la dirección de búsqueda con la dirección seleccionada
        if (!$scope.globalDoc.searchAddress) {
            console.log('no tiene SEARCHADDRESS');
            $scope.globalDoc.searchAddress = {};
        }
        $scope.globalDoc.searchAddress.Street = $scope.globalDoc.currentAddress.address;
        $scope.globalDoc.searchAddress.Latitude = $scope.globalDoc.currentAddress.lat;
        $scope.globalDoc.searchAddress.Longitude = $scope.globalDoc.currentAddress.lng;
        GlobalsService.updateGlobalDoc($scope.globalDoc);

        // Se rellenan los parámetros del ws
        businessObject.lat = $scope.globalDoc.currentAddress.lat;
        businessObject.lng = $scope.globalDoc.currentAddress.lng;
        businessObject.negocio = business.id_negocio;

        // Se muestra un 'cargando..' mientras se recuperan los datos y se pasa de vista
        $ionicLoading.show({
            template: 'Cargando...'
        });
        Business.getDatosRestaurante(businessObject)
            .then(function (response) {
                $ionicLoading.hide();

                var res = response.data.resultado;
                if (res) {
                    if (res.code == 0) {
                        $state.go('app.restaurant', {
                            businessResult: res
                        });
                    } else if (res.code == -3) {
                        $ionicLoading.hide();
                        // Token incorrecto
                        User.refreshData();
                    } else {
                        console.log('KO ir a restaurante');
                        $cordovaDialogs.alert('Servicio temporalmente no disponible', 'Accediendo al restaurante', 'OK');
                    }
                }
            }, function () {
                $ionicLoading.hide();
                console.log('getDatosRestaurante ERROR');
                $cordovaDialogs.alert('Servicio temporalmente no disponible', 'Accediendo al restaurante', 'OK');
            });
    };
})

.controller('MyCountryCtrl', function($scope, GlobalsService, tmhDynamicLocale, $ionicLoading, $timeout, $ionicHistory, $state, Business) {
    $scope.selectedCountry = {};
    var globalDoc = GlobalsService.getGlobalDoc();

    if (globalDoc.countryIsSelected) {
        $scope.selectedCountry.country = globalDoc.selectedCountry;
    }

    $scope.confirmCountry = function () {
        // Se comprueba que venga el parámetro country, es decir, que no haya clicado en 'Selecciona un país'
        if ($scope.selectedCountry.country) {
            globalDoc.countryIsSelected = true;
            // Se actualiza el país actual
            globalDoc.selectedCountry = $scope.selectedCountry.country;
            GlobalsService.updateGlobalDoc(globalDoc);
            // Se define el locale a usar
            tmhDynamicLocale.set($scope.selectedCountry.country.completeCode);

            // Se vuelve a llamar al ws para obtener la info de contacto correcto
            Business.getContacto()
                .then(function (response) {
                    var res = response.data.resultado;
                    if (res.code == 0) {
                        globalDoc.contacto = res.contacto;
                        GlobalsService.updateGlobalDoc(globalDoc);
                        setTimeout(function(){
                            console.log("----------------\nredirigiendo!!!")
                            $state.go('app.find-rest');
                        }, 2000);
                    }
                });

            $ionicLoading.show({
                template: 'País actualizado con éxito!'
            });
            $timeout(function() {
                $scope.address = {};
                $ionicLoading.hide();
                $ionicHistory.goBack();
            }, 2000);
        }
    };
})

.controller('UpdateAddressesCtrl', function($scope, GlobalsService, User, $ionicLoading, $timeout, $cordovaDialogs,
                                            NgMap, $rootScope, getAddressComponentsInfo) {
    $scope.$on('modal.shown', function() {
        // Se obtiene una dirección que no esté actualizada
        $scope.oldAddress = angular.copy(User.getOldAddress());

        NgMap.getMap('update-address-map').then(function(map) {
            $scope.map = map;

            clearMap();
        }, function (err) {
            console.log('ERROR CARGANDO NMAP');
            console.log(err);
        });
    });

    var geocoder = new google.maps.Geocoder;

    var clearMap = function () {
        $scope.map.setCenter(new google.maps.LatLng(40.420146328111386, -5.7038846846558044));
        $scope.map.setZoom(4);
    };

    // Función para borrar y limpiar los campos y cerrar la ventana modal
    $scope.clearModalAndExit = function () {
        $scope.obj = {};
        $scope.envelope = {};
        $scope.updateAddressesModal.hide();
        $scope.showUpdateAddressMap = false;
    };

    $scope.obj = {};
    $scope.envelope = {};
    $scope.showUpdateAddressMap = false;

    var updateOldAddressObject = function (place) {
        $scope.oldAddress.address = place.formatted_address;
        $scope.oldAddress.lat = place.geometry.location.lat();
        $scope.oldAddress.lng = place.geometry.location.lng();

        angular.extend($scope.oldAddress, getAddressComponentsInfo(place.address_components));
    };

    $scope.placeChanged = function() {
        $scope.showUpdateAddressMap = true;
        $scope.obj.place = this.getPlace();

        window.setTimeout(function(){
            google.maps.event.trigger($scope.map, 'resize'); // resize the map with a short delay
            $scope.map.setZoom(17);
            $scope.map.setCenter($scope.obj.place.geometry.location);
        },100);

        // Se actualiza la información de la dirección
        updateOldAddressObject($scope.obj.place);
    };

    $scope.getMarkerLocation = function () {
        // Se obtienen las coordenadas del marker que se ha desplazado
        $scope.obj.place.geometry.location = this.getPosition();
        // Se centra el mapa en dicho punto
        $scope.map.setCenter($scope.obj.place.geometry.location);

        // Se llama al servicio de geocodificación inversa, para obtener los datos de la dirección a partir de coordenadas
        geocoder.geocode({'location': $scope.obj.place.geometry.location}, function(results, status) {
            if (status === google.maps.GeocoderStatus.OK) {
                if (results[0]) {
                    $scope.obj.address = results[0].formatted_address;
                    $scope.envelope.address = results[0].formatted_address;
                    $scope.obj.place.formatted_address = results[0].formatted_address;

                    // Se actualiza la información de la dirección
                    updateOldAddressObject(results[0]);

                    // Se fuerza a actualizar el scope ya que hay algún problema por el cual no actualiza el digest cycle
                    $scope.$apply();
                } else {
                    console.log('No results found');
                }
            } else {
                console.log('Geocoder failed due to: ' + status);
            }
        });
    };

    $scope.updateOldAddress = function () {
        // Se le añade la acción de actualizar
        $scope.oldAddress.accion = 1;

        $ionicLoading.show({
            template: 'Actualizando dirección...'
        });

        User.setDireccion($scope.oldAddress)
            .then(function (response) {
                $ionicLoading.hide();
                var res = response.data.resultado;
                if (res) {
                    // Si el usuario no existe (-1), o hay error en el token (-5), se deslogea al usuario
                    if (res.code == -1 || res.code == -5) {
                        $ionicLoading.show({
                            template: 'Sesión iniciada en otro dispositivo: reiniciando..'
                        });
                        User.refreshData();
                        $timeout(function() {
                            $ionicLoading.hide();
                        }, 2000);
                    } else if (res.code == -4) { //Error en la llamada. No se incluyen datos necesarios
                        $cordovaDialogs.alert('Datos incompletos', 'Actualizar dirección', 'OK');
                    } else if (res.code == -3) { // Dirección fuera de reparto
                        $cordovaDialogs.alert('Lo sentimos, pero de momento no repartimos en esta zona',
                            'Dirección fuera de reparto', 'OK');
                    } else if (res.code == 0) {
                        // Se limpia la dirección y se actualiza con la información recibida
                        delete $scope.oldAddress.accion;
                        delete $scope.oldAddress.$$hashKey;
                        delete $scope.oldAddress.token;

                        $scope.oldAddress.id_city = res.id_ciudad;

                        // Se busca el objeto con el id_direccion en el array de direcciones del usuario
                        for (var i = 0; i < $scope.globalDoc.user.direcciones.length; i++) {
                            if ($scope.globalDoc.user.direcciones[i].id_direccion == res.id.toString()) {
                                $scope.globalDoc.user.direcciones[i] = $scope.oldAddress;
                                GlobalsService.updateGlobalDoc($scope.globalDoc);
                                break;
                            }
                        }

                        // Se actualiza la dirección actual
                        $scope.globalDoc.currentAddress = $scope.oldAddress;
                        GlobalsService.updateGlobalDoc($scope.globalDoc);

                        // Se limpia el mapa y direcciones
                        clearMap();
                        delete $scope.obj.address;
                        delete $scope.envelope.address;
                        delete $scope.obj.place;

                        // Se obtiene una dirección que no esté actualizada
                        $scope.oldAddress = User.getOldAddress();

                        if ($scope.oldAddress) {
                            $ionicLoading.show({
                                template: 'Dirección actualizada con éxito!'
                            });
                        } else {
                            $ionicLoading.show({
                                template: 'Todas las direcciones actualizadas con éxito!'
                            });
                            $scope.updateAddressesModal.hide();
                        }

                        $timeout(function() {
                            $ionicLoading.hide();
                        }, 2000);
                    }
                }
            }, function () {
                console.log('SET DIRECCION KO');
                $ionicLoading.hide();
                $cordovaDialogs.alert('Servicio temporalmente no disponible', 'Actualizar dirección', 'OK');
            });
    };

    $scope.removeOldAddress = function () {
        $cordovaDialogs.confirm("¿Seguro que quieres borrar la dirección '" + $scope.oldAddress.direccion +"'?",
            'Eliminar dirección', ['Sí, borrar', 'No, cancelar'])
            .then(function(buttonIndex) {
                // Si escoge Sí, borrar
                if (buttonIndex == 1) {
                    $ionicLoading.show({
                        template: 'Eliminando dirección...'
                    });

                    // Se le añade al objeto la acción realizada
                    $scope.oldAddress.accion = 2;

                    User.setDireccion($scope.oldAddress)
                        .then(function (response) {
                            $ionicLoading.hide();
                            var res = response.data.resultado;
                            if (res) {
                                // Si el usuario no existe (-1), o hay error en el token (-5), se deslogea al usuario
                                if (res.code == -1 || res.code == -5) {
                                    $ionicLoading.show({
                                        template: 'Sesión iniciada en otro dispositivo: reiniciando..'
                                    });
                                    User.refreshData();
                                    $timeout(function() {
                                        $ionicLoading.hide();
                                    }, 2000);
                                } else if (res.code == -4) { //Error en la llamada. No se incluyen datos necesarios
                                    $cordovaDialogs.alert('Datos incompletos', 'Eliminar dirección', 'OK');
                                } else if (res.code == 0) {
                                    var idDireccion = $scope.oldAddress.id_direccion;
                                    $scope.oldAddress = {};
                                    delete $scope.globalDoc.currentAddress;

                                    // Se busca el objeto con el id_direccion en el array de direcciones del usuario
                                    // para borrarlo
                                    for (var i = 0; i < $scope.globalDoc.user.direcciones.length; i++) {
                                        if ($scope.globalDoc.user.direcciones[i].id_direccion == idDireccion.toString()) {
                                            $scope.globalDoc.user.direcciones.splice(i, 1);
                                            GlobalsService.updateGlobalDoc($scope.globalDoc);
                                            break;
                                        }
                                    }

                                    // Se coge como dirección actual la primera que haya
                                    if ($scope.globalDoc.user.direcciones.length) {
                                        $scope.globalDoc.currentAddress = $scope.globalDoc.user.direcciones[0];
                                        GlobalsService.updateGlobalDoc($scope.globalDoc);
                                    }
                                    $ionicLoading.show({
                                        template: 'Dirección eliminada con éxito!'
                                    });

                                    // Se limpia el mapa y direcciones
                                    clearMap();
                                    delete $scope.obj.address;
                                    delete $scope.envelope.address;
                                    delete $scope.obj.place;

                                    // Se obtiene una dirección que no esté actualizada
                                    $scope.oldAddress = User.getOldAddress();
                                    $timeout(function() {
                                        if (!$scope.oldAddress) {
                                            $scope.updateAddressesModal.hide();
                                        }
                                        $ionicLoading.hide();
                                    }, 2000);
                                }
                            }
                        }, function () {
                            console.log('SET DIRECCION KO');
                            $ionicLoading.hide();
                            $cordovaDialogs.alert('Servicio temporalmente no disponible', 'Eliminar dirección', 'OK');
                        });
                }
            });
    };

    // Función que se dispara cuando gana el foco el input, para deshabilitar el 'data-tap-disabled' de Ionic
    // que hace que el Autocomplete de Google Places no funcione bien en iOS. OJO! No es igual que la de la home porque
    // hay que aplicar un fix para que funcione en ventanas modales
    $scope.disableTap = function(event) {
        var input = event.target;

        // Get the predictions element
        var container = document.getElementsByClassName('pac-container');
        container = angular.element(container);

        // Apply css to ensure the container overlays the other elements, and
        // events occur on the element not behind it
        container.css('z-index', '5000');
        container.css('pointer-events', 'auto');

        // Disable ionic data tap
        container.attr('data-tap-disabled', 'true');

        // Leave the input field if a prediction is chosen
        container.on('click', function(){
            input.blur();
        });
    };
})
.controller('CloseSessionCtrl', function($scope,$stateParams, $state,$ionicHistory) {
    $scope.okCloseSession = function(e){
        localStorage.removeItem('API_URL');
        localStorage.removeItem('aloha24DB');
        localStorage.removeItem('appVersion');
        /*
        $ionicHistory.clearHistory();
            $ionicHistory.clearCache()
                .then(function () {
                    $ionicHistory.nextViewOptions({
                        historyRoot: true
                    });
                    $state.go('app.find-rest');
                });
        */
        setTimeout(function(){
            window.location.reload(true);
        },200);
        $state.transitionTo('app.find-rest',null,{reload: true,inherit: false, notify: true});
        
    }
})
.controller('ExitCtrl', function($scope, $stateParams,$state) {
    $scope.okExitApp = function(e){
        if (navigator && navigator.app) {
            navigator.app.exitApp();
        } else
            if (navigator && navigator.device)
                navigator.device.exitApp();
    }
});
