
angular.module('aloha24', ['ionic', 'ngCordova', 'app.controllers', 'app.globals', 'app.routes', 'app.directives', 'app.constants', 'app.user',
        'app.services','lokijs', 'ion-affix', 'jett.ionic.filter.bar', 'angular-md5', 'angularMoment',
        'ionic-datepicker', 'ui.bootstrap', 'ngMap', 'ngAnimate', 'tmh.dynamicLocale'])

.filter('trimSeconds', function() {
    return function (time) {
        return time.replace(/:00+$/, '');
    };
})

.filter('removeHTMLTags', function() {
    return function(text) {
        return String(text).replace(/<[^>]+>/gm, '');
    };
})

.filter('isEmpty', function () {
    var bar;
    return function (obj) {
        for (bar in obj) {
            if (obj.hasOwnProperty(bar)) {
                return false;
            }
        }
        return true;
    };
})

.run(function($ionicPlatform, GlobalsService, $ionicHistory, User, $rootScope, $cordovaDialogs) {
    $ionicPlatform.ready(function() {
        // Mostrar Accessory Bar para que salga el 'Done', moverse a siguiente campo, etc
        if (window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(false);
            cordova.plugins.Keyboard.disableScroll(false);
        }
        if (window.StatusBar) {
            // org.apache.cordova.statusbar required
            StatusBar.styleDefault();
        }

        // Se inicializa la BD local
        GlobalsService.initDB();

        // En el browser no está definido..
        if (window.FirebasePlugin) {
            console.log('FirebasePlugin definido');
            // Pide permiso para recibir notificaciones. Necesario para que funcionen las push en iOS
            window.FirebasePlugin.grantPermission();

            window.FirebasePlugin.getToken(function(token) {
                console.log(token);

                // save this server-side and use it to push notifications to this device
                User.auth.push = token;
            }, function(error) {
                console.error(error);
            });

            window.FirebasePlugin.onTokenRefresh(function(token) {
                console.log('on token refreshed');
                console.log(token);

                // save this server-side and use it to push notifications to this device
                User.auth.push = token;
            }, function(error) {
                console.error(error);
            });

            window.FirebasePlugin.onNotificationOpen(function(notification) {
                console.log('onNotificationOpen por la del appjs');
                console.log(JSON.stringify(notification));

                var body = '';
                var title = 'Notificación';

                if (device.platform.toLowerCase() == 'ios') {
                    console.log('PUSH ios');
                    try {
                        console.log(typeof notification.aps);
                        console.log(typeof notification.aps.alert);

                        if (notification.aps && notification.aps.alert) {
                            // En este caso sólo tenemos mensaje sin título
                            if (typeof notification.aps.alert === 'string') {
                                body = notification.aps.alert;
                            } else {
                                title = notification.aps.alert.title;
                                body = notification.aps.alert.body;
                            }
                        }
                    } catch (e) {
                        console.log('Error trying to reconstruct push ios: ' + e);
                    }
                } else if (device.platform.toLowerCase() == 'android') {
                    console.log('PUSH android');
                    if (notification.body) {
                        body = notification.body;
                    }
                    if (notification.title) {
                        title = notification.title;
                    }
                }

                if (body !== '') {
                    $cordovaDialogs.alert(body, title, 'OK');
                }
            }, function(error) {
                console.error(error);
            });

        }
    });

    // Se modifica el comportamiento del botón atrás de Android
    $ionicPlatform.registerBackButtonAction(function (event) {
        // Se deshabilita en la vista del restaurante
        if ($ionicHistory.currentStateName() === 'app.restaurant'){
            event.preventDefault();
        } else {
            $ionicHistory.goBack();
        }
    }, 100);
});
