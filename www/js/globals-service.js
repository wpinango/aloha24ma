angular.module('app.globals', [])

.factory('GlobalsService', function($http, Loki, $rootScope,apiConstants) {
    var _db;
    var _globals;
    var _globalDoc;
    var countries = [
        {
            label: 'España',
            code: 'es',
            completeCode: 'es-es',
            protocol: 'http',
            url:'aloha24.com/alohages/ws/aloha24'
        }/*,
        {
            label: 'Ecuador',
            code: 'ec',
            completeCode: 'es-ec',
            protocol: 'https',
            url:'aloha24.ec/alohages/ws/aloha24'//aloha24.ec\/alohages\/ws\/aloha24
        }/*,
        {
            label: 'Honduras',
            code: 'hn',
            completeCode: 'es-hn',
            protocol: 'http',
            url:'aloha24.hn/alohages/ws/aloha24'
        }*/
        /*{
            label: 'Panamá',
            code: 'pa',
            completeCode: 'es-pa'
        },*/
        
    ];

    var loadHandler = function () {
        console.log('aloha24DB LokiJS DB loaded');
        // if database did not exist it will be empty so I will intitialize here
        _globals = _db.getCollection('globals');
        console.log('Global ' + JSON.stringify(_globals))
        if (_globals === null) {
            console.log('no existe la colección globals');
            _globals = _db.addCollection('globals');
            _globalDoc = _globals.insert({ tag : 'aloha', user: {}, countries: countries});
            console.log('test ' + JSON.stringify(_globals.findOne({ tag : 'aloha'})));
        } else {
            console.log('ya existe la colección globals');
            _globalDoc = _globals.findOne({ tag : 'aloha'});            

            // Se comprueba si tiene los países incluidos, en caso contrario se incluyen
            if (!_globalDoc.countries) {
                _globalDoc.countries = countries;
            }
        }

        //$rootScope.$emit('dbLoaded', _globalDoc);
        $rootScope.$broadcast('dbLoaded', _globalDoc);

    };

    return {
        initDB: function() {
            console.log('Iniciando LokiJS DB');
            //var adapter = new LokiCordovaFSAdapter({"prefix": "loki"});
            _db = new Loki('aloha24DB',
                {
                    autoload: true,
                    autoloadCallback : loadHandler,
                    autosave: true,
                    autosaveInterval: 1000 // 1 second
              //      adapter: adapter
                });
        },

        getGlobalDoc: function() {
            return _globalDoc;
        },

        updateGlobalDoc: function(globalDoc) {
            _globals.update(globalDoc);
            //window.localStorage.setItem('API_URL', globalDoc.selectedCountry.protocol + '://' + globalDoc.selectedCountry.url);
        },

        setCountries: function(countries){
            var vCountries = [];
            countries.forEach(function(country){
                var cCode = country[2].substring(country[2].indexOf('-')+1,country[2].length);
                vCountries.push({label:country[0],code:cCode,completeCode:country[2],url:country[1],protocol:country[3].toLowerCase()});
            });
            _globalDoc.countries = vCountries;
            return _globalDoc.countries;
        }
    };
});