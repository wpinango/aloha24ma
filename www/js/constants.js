angular.module('app.constants', [])
.constant('apiConstants', {
        //v010500 - v030000 - v040000 - v050000
        'appVersionbrowser': '020503',
        'appVersionios': '020503',
        'appVersionandroid': '020503',
        //'apiUrl': 'http://51.254.215.45/aloha24',  // Desarrollo
        //'apiUrl': 'http://51.254.215.45/alohages/ws/aloha24',  // NUEVA url Desarrollo
        //'apiUrl': 'http://aloha24.com/delivery', // Prod
        'apiUrl': 'aloha24.com/alohages/ws/aloha24', // NUEVA url prod
        //'apiUrl' : 'http://aloha24.hn/alohages/ws/aloha24',
        'wsLogin': '/?/ws_getAuth.php',
        'wsRegister': '/?/ws_setUser.php',
        'wsUpdateUser': '/?/ws_updUser.php',
        'wsGetCiudades': '/?/ws_getCiudades.php',
        'wsGetListaNegocios': '/?/ws_getListaNegocios.php',
        'wsGetDatosRestaurante': '/?/ws_getDatosNegocio.php',
        'wsGetContacto': '/?/ws_getContacto.php',
        'wsSetMahalos': '/?/ws_setMahalos.php',
        'wsGetDescuentos': '/?/ws_getDescuentos.php',
        'wsGetFavoritos': '/?/ws_getFavoritos.php',
        'wsSetFavorito': '/?/ws_setFavorito.php',
        'wsSetDireccion': '/?/ws_setDireccion.php',
        'wsGetCuponValido': '/?/ws_getCuponValido.php',
        'wsSetPedido': '/?/ws_setPedido.php',
        'wsRecoverPass': '/?/ws_setPass.php',
        'wsSetPush': '/?/ws_setPushService.php',
        'wsGetVersion': '/ws_getVersion.php',
        'wsGetPedido': '/?/ws_getPedido.php',
        'wsCheckAddress': '/?/ws_chkAddress.php',
        'wsLogout': '/?/ws_setLogout.php'
    }
)
.constant('currencySettings', {
        //'symbol': '€',
        'fractionSize': 2
})
.constant('shopSettings', {
        payPalSandboxId : 'Ab_SvJzrn-hzEbHbynsWFlp2dnPxogVTq9hpZtHOwyozvL3qmdwm5m2u2HcX6jrpPfr_g7AtvUSqtB7M',
        payPalProductionId : 'AfJ58RCVAmDsJ58YG13FOVFgAedAcCSo-MtRq-7IIV2i95zqZSYsneIvb1Br',
        payPalEnv: 'PayPalEnvironmentProduction',   // 'PayPalEnvironmentSandbox' for testing .  'PayPalEnvironmentProduction' for production
        //payPalEnv: 'PayPalEnvironmentSandbox',
        payPalShopName : 'Aloha24',
        payPalMerchantPrivacyPolicyURL : 'https://www.aloha24.com/aloha/public/policy',
        payPalMerchantUserAgreementURL : 'https://www.aloha24.com/aloha/public/policy'
    }
);