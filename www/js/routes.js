angular.module('app.routes', ['jett.ionic.filter.bar', 'ionic-datepicker', 'tmh.dynamicLocale'])

.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider, $ionicFilterBarConfigProvider,
                 ionicDatePickerProvider, tmhDynamicLocaleProvider) {
    $ionicConfigProvider.backButton.previousTitleText(false);
    $ionicConfigProvider.backButton.text('');
    $ionicConfigProvider.navBar.alignTitle('center');

    $ionicFilterBarConfigProvider.theme('calm');
    $ionicFilterBarConfigProvider.placeholder('Busca restaurantes');

    tmhDynamicLocaleProvider.localeLocationPattern('lib/angular-i18n/angular-locale_{{locale}}.js');
    tmhDynamicLocaleProvider.defaultLocale('es-es');

    var datePickerObj = {
        inputDate: new Date(),
        setLabel: 'Guardar',
        todayLabel: 'Hoy',
        closeLabel: 'Cerrar',
        mondayFirst: true,
        weeksList: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
        monthsList: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
        templateType: 'popup',
        showTodayButton: false,
        dateFormat: 'dd MMMM yyyy',
        closeOnSelect: false
    };
    ionicDatePickerProvider.configDatePicker(datePickerObj);

    $stateProvider
        .state('app', {
            url: '/app',
            abstract: true,
            templateUrl: 'templates/menu.html',
            controller: 'AppCtrl'
        })
        .state('login', {
            url: '/login',
            params: {
                sourceView: null
            },
            templateUrl: 'templates/login.html',
            controller: 'LoginCtrl'
        })
        .state('register', {
            url: '/register',
            params: {
                sourceView: null
            },
            templateUrl: 'templates/register.html',
            controller: 'LoginCtrl'
        })
        .state('app.profile', {
            url: '/profile',
            views: {
                'menuContent': {
                    templateUrl: 'templates/profile.html',
                    controller: 'ProfileCtrl'
                }
            }
        })
        .state('app.find-rest', {
            url: '/find-rest',
            views: {
                'menuContent': {
                    templateUrl: 'templates/find-rest.html'
                }
            }
        })
        .state('app.search-result', {
            url: '/result',
            params: {
                searchResult: null
            },
            views: {
                'menuContent': {
                    templateUrl: 'templates/search-result.html',
                    controller: 'SearchResultCtrl'
                }
            }
        })
        .state('app.restaurant', {
            url: '/restaurant',
            params: {
                businessResult: null
            },
            views: {
                'menuContent': {
                    templateUrl: 'templates/restaurant.html',
                    controller: 'RestaurantCtrl'
                }
            }
        })
        .state('app.restaurant-info', {
            url: '/restaurant-info',
            params: {
                negocio: null
            },
            views: {
                'menuContent': {
                    templateUrl: 'templates/restaurant-info.html',
                    controller: 'RestaurantInfoCtrl'
                }
            }
        })
        .state('app.dish-details', {
            url: '/dish-details',
            params: {
                producto: null,
                negocioIndex: -1
            },
            views: {
                'menuContent': {
                    templateUrl: 'templates/dish-details.html',
                    controller: 'DishDetailsCtrl'
                }
            },
            cache: false
        })

        .state('app.cart', {
            url: '/cart',
            views: {
                'menuContent': {
                    templateUrl: 'templates/order-cart.html',
                    controller: 'OrderCartCtrl'
                }
            },
            cache: false
        })
        .state('app.summary', {
            url: '/summary',
            views: {
                'menuContent': {
                    templateUrl: 'templates/order-summary.html',
                    controller: 'OrderCartCtrl'
                }
            },
            cache: false
        })
        .state('app.my-orders', {
            url: '/my-orders',
            views: {
                'menuContent': {
                    templateUrl: 'templates/my-orders.html',
                    controller: 'MyOrdersCtrl'
                }
            },
            resolve: {
                pedidos: function(GlobalsService) {
                    return GlobalsService.getGlobalDoc().user.pedidos || null;
                }
            },
            cache: false
        })
        .state('app.mahalos', {
            url: '/mahalos',
            views: {
                'menuContent': {
                    templateUrl: 'templates/mahalos.html',
                    controller: 'MahalosCtrl'
                }
            },
            resolve: {
                pedidos: function(GlobalsService) {
                    return GlobalsService.getGlobalDoc().user.pedidos || null;
                }
            }
        })
        .state('app.discounts', {
            url: '/discounts',
            views: {
                'menuContent': {
                    templateUrl: 'templates/discounts.html',
                    controller: 'DiscountsCtrl'
                }
            },
            resolve: {
                descuentos: function(GlobalsService) {
                    return GlobalsService.getGlobalDoc().user.descuentos || null;
                }
            }
        })
        .state('app.favourites', {
            url: '/favourites',
            views: {
                'menuContent': {
                    templateUrl: 'templates/favourites.html',
                    controller: 'FavouritesCtrl'
                }
            },
            resolve: {
                favoritos: function(GlobalsService) {
                    return GlobalsService.getGlobalDoc().user.favoritos || null;
                }
            }
        })
        .state('app.contact', {
            url: '/contact',
            views: {
                'menuContent': {
                    templateUrl: 'templates/contact.html',
                    controller: 'ContactCtrl'
                }
            },
            resolve: {
                contact: function(GlobalsService) {
                    return GlobalsService.getGlobalDoc().contacto || null;
                }
            }
        })

        .state('app.my-addresses', {
            url: '/my-addresses',
            views: {
                'menuContent' : {
                    templateUrl: 'templates/my-addresses.html',
                    controller: 'MyAddressesCtrl'
                }
            },
            cache: false
        })

        .state('app.my-country', {
            url: '/my-country',
            views: {
                'menuContent' : {
                    templateUrl: 'templates/my-country.html',
                    controller: 'MyCountryCtrl'
                }
            },
            cache: false
        })

        .state('app.order-success', {
            url: '/order-success',
            views: {
                'menuContent' : {
                    templateUrl: 'templates/order-success.html',
                    controller: 'OrderSuccessCtrl'
                }
            }
        })

        .state('app.order-address', {
            url: '/order-address',
            views: {
                'menuContent' : {
                    templateUrl: 'templates/order-address.html',
                    controller: 'OrderAddressCtrl'
                }
            }
        })

        .state('app.closeSession', {
            url: '/closeSession',
            views: {
                'menuContent' : {
                    templateUrl: 'templates/closeSession.html',
                    controller: 'CloseSessionCtrl'
                }
            }
        })
        .state('app.exitApp', {
            url: '/exitApp',
            views: {
                'menuContent' : {
                    templateUrl: 'templates/exitApp.html',
                    controller: 'ExitCtrl'
                }
            }
        })
        ;
    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/app/find-rest');
});