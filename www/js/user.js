angular.module('app.user', [])

.factory('User', function($http, apiConstants, transformRequestAsFormPost, wrapDataToWsExpectedScheme, $state,
                          $ionicHistory, GlobalsService, Business, $q, $ionicLoading, $timeout) {
    var auth = {
        isLogged: false,
        dbLoaded: false,
        hasPushRegistered: false,
        token: '-1',
        push: '-1' 
    };    

    var countries = {
        multipleCountries: true
    };
    var pedido = {
        referencia: "",
        info_pedido_cliente: {
            id_descuentos_globales: "",
            id_forma_pago: 3,
            comentarios: "",
            id_descuento_cliente: "",
            id_paypal: ""
        },
        pedido: [],
        total_pedido: 0,
        descuento: ""
    };

    return {
        login: function(credentials) {
            var wsUrl = apiConstants.wsLogin;
            if (wsUrl.indexOf('?') != -1) {
                wsUrl = wsUrl.replace('?', Business.getApiVersion());
            }
            console.log(window.localStorage.getItem('API_URL') + wsUrl);
            return $http.post(window.localStorage.getItem('API_URL') + wsUrl, wrapDataToWsExpectedScheme(credentials), {
                headers : {
                    'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
                },
                transformRequest: transformRequestAsFormPost
            });
        },

        register: function(credentials) {
            // TEMPORAL HASTA QUE DEJEN DE SER OBLIGATORIOS ESOS DATOS
/*
            credentials.direccion = "Calle San Roque";
            credentials.cp = "33009";
            credentials.id_ciudad = "2";
*/
            // /TEMPORAL

            // Se define el nivel de API que hay que atacar
            var wsUrl = apiConstants.wsRegister;
            if (wsUrl.indexOf('?') != -1) {
                wsUrl = wsUrl.replace('?', Business.getApiVersion());
            }

            return $http.post(window.localStorage.getItem('API_URL') + wsUrl, wrapDataToWsExpectedScheme(credentials), {
                headers : {
                    'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
                },
                transformRequest: transformRequestAsFormPost
            });
        },
        
        updateUser: function (userObject) {
            // Se define el nivel de API que hay que atacar
            var wsUrl = apiConstants.wsUpdateUser;
            if (wsUrl.indexOf('?') != -1) {
                wsUrl = wsUrl.replace('?', Business.getApiVersion());
            }

            // Se le añade el token de autenticación
            userObject.token = auth.token;
            return $http.post(window.localStorage.getItem('API_URL') + wsUrl, wrapDataToWsExpectedScheme(userObject), {
                headers : {
                    'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
                },
                transformRequest: transformRequestAsFormPost
            });
        },

        recoverPass: function (recoverPassObj) {
            var wsUrl = apiConstants.wsRecoverPass;
            if (wsUrl.indexOf('?') != -1) {
                wsUrl = wsUrl.replace('?', Business.getApiVersion());
            }

            return $http.post(window.localStorage.getItem('API_URL') + wsUrl, wrapDataToWsExpectedScheme(recoverPassObj), {
                headers : {
                    'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
                },
                transformRequest: transformRequestAsFormPost
            });
        },

        // Se devuelve el objeto del negocio en el pedido y su índice
        getNegocioInPedidoById: function (negocioId) {
            for (var i = 0; i < pedido.pedido.length; i++) {
                if (pedido.pedido[i].id_negocio == negocioId) {
                    // Se devuelve el objeto y el índice del mismo
                    return [pedido.pedido[i], i];
                }
            }
            return null;
        },

        // Se devuelve el objeto del producto en el pedido (pásandole el negocio correspondiente) y su índice
        getProductInPedidoById: function (negocioIndex, productId) {
            // Se obtiene el negocio dentro del pedido
            var negocio = pedido.pedido[negocioIndex];
            // Se recorre el array que contiene los productos añadidos
            for (var i = 0; i < negocio.productos.length; i++) {
                if (negocio.productos[i].id_producto == productId) {
                    // Se devuelve el objeto y el índice del mismo
                    return [negocio.productos[i], i];
                }
            }

            // Si no se encuentra ninguno se devuelve 'null'
            return null;
        },

        updateTotalPedido: function () {
            var totalPedido = 0;
            // Se actualiza el total del pedido recorriendo cada negocio-restaurante
            for (var i = 0; i < pedido.pedido.length; i++) {
                // Si hay productos añadidos al pedido en este restaurante
                if (pedido.pedido[i].productos.length > 0 && pedido.pedido[i].total_restaurante > 0) {
                    totalPedido = totalPedido + pedido.pedido[i].total_restaurante;
                }
            }
            pedido.total_pedido = totalPedido;
        },

        getTotalOrderTransportCost: function () {
            var totalOrderTransportCost = 0;

            for (var i = 0; i < pedido.pedido.length; i++) {
                // Sólo se suma el transporte de restaurantes con productos
                if (pedido.pedido[i].productos.length > 0) {
                    totalOrderTransportCost = totalOrderTransportCost + parseFloat(pedido.pedido[i].transporte);
                }
            }
            return totalOrderTransportCost;
        },

        refreshOrderTransportCost: function (cp) {
            // Parámetro por defecto a 0, por si no se le pasa en la función
            cp = cp || 0;
            // Se recorren los negocios del pedido para actualizar su transporte
            for (var i = 0; i < pedido.pedido.length; i++) {
                // Se actualiza el transporte
                //pedido.pedido[i].transporte = Business.getTransportCost(pedido.pedido[i].ctrans, cp);
                //pedido.pedido[i].transporte = pedido.pedido[i].transporte;
                // Se actualiza el total del restaurante
                var totalImporteProductos = 0;
                // Por cada producto se actualiza el total del importe de este negocio
                for(var j = 0; j < pedido.pedido[i].productos.length; j++) {
                    totalImporteProductos = totalImporteProductos + (pedido.pedido[i].productos[j].importe * pedido.pedido[i].productos[j].cantidad);
                }
                pedido.pedido[i].total_restaurante = totalImporteProductos + parseFloat(pedido.pedido[i].transporte);
            }


            this.updateTotalPedido();
        },

        isTransportCostVariable: function () {
            // Se recorren los negocios del pedido para comprobar si alguno tiene coste de transporte variable
            for (var i = 0; i < pedido.pedido.length; i++) {
                if (typeof pedido.pedido[i].ctrans === 'number') {
                    pedido.pedido[i].ctrans = pedido.pedido[i].ctrans.toString();
                }
                // Sólo compruebo los restaurantes con productos añadidos
                if (pedido.pedido[i].productos.length > 0 && pedido.pedido[i].ctrans.indexOf(',') != -1) {
                    return true;
                }
            }
            return false;
        },

        clearPedido: function () {
            pedido.total_pedido = 0;
            pedido.descuento = "";
            pedido.referencia = "";

            for (var i = pedido.pedido.length; i > 0; i--) {
                pedido.pedido.pop();
            }

            // TODO: limpiar info_pedido_cliente, para que se limpien también las referencias
        },

        cleanPedido: function () {
            // Se limpian todos los negocios que no tengan productos
            for (var i = pedido.pedido.length; i > 0; i--) {
                if (pedido.pedido[i-1].productos.length == 0) {
                    pedido.pedido.splice(i-1, 1);
                }
            }
        },

        logout: function () {
            // Limpiar el user del 'local storage', historial y caché
            // TODO: comprobar si hay que borrar más cosas, el usuario entero?
            var globalDoc = GlobalsService.getGlobalDoc();
            //globalDoc.user.token = "";
            globalDoc.user = {};
            if (globalDoc.currentAddress) {
                globalDoc.currentAddress = {};
            }
            if (globalDoc.hideInitialMessage) {
                globalDoc.hideInitialMessage = false;
            }
            GlobalsService.updateGlobalDoc(globalDoc);

            var pushObject = {};
            pushObject.token = auth.token;

            // Hacer logout al usuario
            auth.isLogged = false;
            auth.token = '-1';
            Business.setAuthToken('-1');

            // Limpiar el pedido
            this.clearPedido();

            var wsUrl = apiConstants.wsLogout;
            if (wsUrl.indexOf('?') != -1) {
                wsUrl = wsUrl.replace('?', Business.getApiVersion());
            }
            // Desvincular el dispositivo para no recibir PUSH
            $http.post(window.localStorage.getItem('API_URL') + wsUrl, wrapDataToWsExpectedScheme(pushObject), {
                headers : {
                    'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
                },
                transformRequest: transformRequestAsFormPost
            });

            // Se borra de Firebase para que no reciba Push
            window.FirebasePlugin.unregister();


            $ionicHistory.clearHistory();
            $ionicHistory.clearCache()
                .then(function () {
                    $ionicHistory.nextViewOptions({
                        historyRoot: true
                    });
                    $state.go('app.find-rest');
                });
        },

        getFavoritos: function () {
            var wsUrl = apiConstants.wsGetFavoritos;
            if (wsUrl.indexOf('?') != -1) {
                wsUrl = wsUrl.replace('?', Business.getApiVersion());
            }

            // Se le añade el token de autenticación
            var favoritosObj = {};
            favoritosObj.token = auth.token;
            return $http.post(window.localStorage.getItem('API_URL') + wsUrl, wrapDataToWsExpectedScheme(favoritosObj), {
                headers : {
                    'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
                },
                transformRequest: transformRequestAsFormPost
            });
        },

        setFavorito: function (favObj) {
            var wsUrl = apiConstants.wsSetFavorito;
            if (wsUrl.indexOf('?') != -1) {
                wsUrl = wsUrl.replace('?', Business.getApiVersion());
            }

            // Se le añade el token de autenticación
            favObj.token = auth.token;
            return $http.post(window.localStorage.getItem('API_URL') + wsUrl, wrapDataToWsExpectedScheme(favObj), {
                headers : {
                    'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
                },
                transformRequest: transformRequestAsFormPost
            });
        },

        setDireccion: function (addressObj) {
            var wsUrl = apiConstants.wsSetDireccion;
            if (wsUrl.indexOf('?') != -1) {
                wsUrl = wsUrl.replace('?', Business.getApiVersion());
            }

            // Se le añade el token de autenticación
            addressObj.token = auth.token;
            return $http.post(window.localStorage.getItem('API_URL') + wsUrl, wrapDataToWsExpectedScheme(addressObj), {
                headers : {
                    'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
                },
                transformRequest: transformRequestAsFormPost
            });
        },

        getCuponValido: function (cuponObj) {
            var wsUrl = apiConstants.wsGetCuponValido;
            if (wsUrl.indexOf('?') != -1) {
                wsUrl = wsUrl.replace('?', Business.getApiVersion());
            }

            // Se le añade el token de autenticación
            cuponObj.token = auth.token;
            return $http.post(window.localStorage.getItem('API_URL') + wsUrl, wrapDataToWsExpectedScheme(cuponObj), {
                headers : {
                    'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
                },
                transformRequest: transformRequestAsFormPost
            });
        },

        setPedido: function (pedidoObj) {
            var wsUrl = apiConstants.wsSetPedido;
            if (wsUrl.indexOf('?') != -1) {
                wsUrl = wsUrl.replace('?', Business.getApiVersion());
            }

            // Se le añade el token de autenticación
            //pedidoObj.token = auth.token;
            return $http.post(window.localStorage.getItem('API_URL') + wsUrl, wrapDataToWsExpectedScheme(pedidoObj), {
                headers : {
                    'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
                },
                transformRequest: transformRequestAsFormPost
            });
        },

        refreshData: function () {
            var globalDoc = GlobalsService.getGlobalDoc();
            // TODO: añadir gestión de errores para controlar si hay datos de usuario en local, etc
            var credentials = {
                username: globalDoc.user.email,
                password: globalDoc.user.pass
            };

            var that = this;
            this.login(credentials)
                .then(function(response) {
                    var res = response.data.resultado;
                    if (res) {
                        if (res.code == 0) {
                            // Se actualiza el usuario persistido en local con los datos recibidos del servidor
                            globalDoc.user = res;
                            // Se guarda la pass en local. ESTO ES UNA CHAMBONADA Y UN AGUJERO DE SEGURIDAD PERO CON LOS WS
                            // EXISTENTES SE NECESITA HACERLO
                            // TODO: quitar esta patraña cuando se pueda hacer
                            globalDoc.user.pass = credentials.password;
                            GlobalsService.updateGlobalDoc(globalDoc);
                            // Se marca el usuario como logeado y se guarda también el token en memoria
                            auth.isLogged = true;
                            auth.token = res.token;
                            Business.setAuthToken(res.token);

                            // Se actualizan los favoritos
                            that.getFavoritos()
                                .then(function (response) {
                                    var res = response.data.resultado;
                                    if (res.code == 0) {
                                        globalDoc.user.favoritos = res.favoritos;
                                        GlobalsService.updateGlobalDoc(globalDoc);
                                        console.log('Datos de usuario actualizados');
                                    }
                                });

                            // Se registra si no se han registrado ya las push
                            if (!auth.hasPushRegistered) {
                                auth.hasPushRegistered = true;
                                that.setPush();
                            }

                        } else {
                            console.log('ERROR: algo fue mal al relogear usuario para refrescar datos');
                        }
                    } else {
                        console.log('ERROR: algo fue mal al relogear usuario para refrescar datos');
                    }
                });
        },

        changeCurrentAddress: function (id_direccion) {
            var globalDoc = GlobalsService.getGlobalDoc();

            // Se comprueba que el usuario tenga direcciones ya metidas
            if (globalDoc.user && globalDoc.user.direcciones) {
                for (var i = 0; i < globalDoc.user.direcciones.length; i++) {
                    if (globalDoc.user.direcciones[i].id_direccion == id_direccion) {
                        console.log('dirección encontrada!');
                        globalDoc.currentAddress = globalDoc.user.direcciones[i];
                        GlobalsService.updateGlobalDoc(globalDoc);
                        return true;
                    }
                }
            }

            return false;
        },

        checkAddress: function (checkAddressObj) {
            var deferred = $q.defer();
            var that = this;

            checkAddressObj.token = auth.token;
            checkAddressObj.negocios = [];

            // Se recorre el pedido en busca de los negocios existentes y de paso se guarda el actual coste de transporte
            var oldTotalOrderTransportCost = 0;
            for (var i = 0; i < pedido.pedido.length; i++) {
                checkAddressObj.negocios.push(pedido.pedido[i].id_negocio);
                // Sólo se suma el transporte de restaurantes con productos
                if (pedido.pedido[i].productos.length > 0) {
                    oldTotalOrderTransportCost = oldTotalOrderTransportCost + parseFloat(pedido.pedido[i].transporte);
                }
            }

            $ionicLoading.show({
                template: 'Calculando...'
            });

            var wsUrl = apiConstants.wsCheckAddress;
            if (wsUrl.indexOf('?') != -1) {
                wsUrl = wsUrl.replace('?', Business.getApiVersion());
            }
            // Se llama al servicio para comprobar el nuevo coste de transporte
            $http.post(window.localStorage.getItem('API_URL') + wsUrl, wrapDataToWsExpectedScheme(checkAddressObj), {
                headers : {
                    'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
                },
                transformRequest: transformRequestAsFormPost
            })
                .then(function (response) {
                    $ionicLoading.hide();
                    var res = response.data.resultado;
                    if (res) {
                        if (res.code == 0) {
                            // En este caso significa que el/los negocio/s reparten en esta dirección
                            // Se recorren los negocios y se actualizan los parámetros de transporte
                            for (var i = 0; i < res.negocios.length; i++) {
                                // Se busca el negocio con el id correspondiente
                                for (var j = 0; j < pedido.pedido.length; j++) {
                                    if (res.negocios[i].id == pedido.pedido[j].id_negocio) {
                                        console.log('negocio encontrado, actualizando coste');
                                        pedido.pedido[j].transporte = res.negocios[i].transport;
                                        pedido.pedido[j].transporteFloat = parseFloat(res.negocios[i].transport);
                                        pedido.pedido[j].free = res.negocios[i].free;
                                        pedido.pedido[j].pedido_minimo = res.negocios[i].minorder;
                                        break;
                                    }
                                }
                            }

                            var newTotalOrderTransportCost = that.getTotalOrderTransportCost();
                            console.log('NUEVO transporte total: ' + newTotalOrderTransportCost);
                            // Si hay un nuevo coste de transporte, se refresca y actualiza el total del pedido
                            if (oldTotalOrderTransportCost != newTotalOrderTransportCost) {
                                that.refreshOrderTransportCost(0);

                                $ionicLoading.show({
                                    template: 'Coste de transporte actualizado'
                                });
                                $timeout(function() {
                                    $ionicLoading.hide();
                                }, 2000);
                            }

                            deferred.resolve();
                        } else {
                            $ionicLoading.show({
                                template: 'El negocio no reparte en esta dirección'
                            });
                            $timeout(function() {
                                $ionicLoading.hide();
                                deferred.reject();
                            }, 2000);
                        }
                    } else {
                        deferred.reject();
                    }
                }, function (err) {
                    $ionicLoading.show({
                        template: 'Ooops ha ocurrido un error..'
                    });
                    $timeout(function() {
                        $ionicLoading.hide();
                        deferred.reject();
                    }, 2000);
                });

            return deferred.promise;
        },

        hasAddressesWithFormat: function (format) { // format: 'new' | 'old' , para saber si tiene alguna dirección nueva, o alguna vieja
            // Si no está logeado se retorna directamente con false
            if (auth.isLogged) {
                var globalDoc = GlobalsService.getGlobalDoc();
                // Se comprueba que el usuario tenga direcciones ya metidas
                if (globalDoc.user && globalDoc.user.direcciones) {
                    for (var i = 0; i < globalDoc.user.direcciones.length; i++) {
                        // Se comprueba si en el objeto de la dirección el campo 'address' está vacío o no
                        if (format == 'old' && !globalDoc.user.direcciones[i].address) {
                            // Si está vacío significa que esa dirección no está actualizada al nuevo formato
                            return true;
                        }
                        if (format == 'new' && globalDoc.user.direcciones[i].address) {
                            // Si no está vacío significa que tiene al menos una dirección actualizada
                            return true;
                        }
                    }
                }
            }
            return false;
        },

        getOldAddress: function () {
            // Si no está logeado se retorna directamente con false
            if (auth.isLogged) {
                var globalDoc = GlobalsService.getGlobalDoc();
                // Se comprueba que el usuario tenga direcciones ya metidas
                if (globalDoc.user && globalDoc.user.direcciones) {
                    for (var i = 0; i < globalDoc.user.direcciones.length; i++) {
                        // Se comprueba si en el objeto de la dirección el campo 'address' está vacío o no
                        if (!globalDoc.user.direcciones[i].address) {
                            // Si está vacío significa que esa dirección no está actualizada al nuevo formato
                            return globalDoc.user.direcciones[i];
                        }
                    }
                }
            }
            return null;
        },

        setPush: function () {
            console.log('PUSH: actualizando push');
            var wsUrl = apiConstants.wsSetPush;
            if (wsUrl.indexOf('?') != -1) {
                wsUrl = wsUrl.replace('?', Business.getApiVersion());
            }

            // Se comprueba si dispone de un token válido para actualizar las PUSH
            if (auth.push != '-1' && auth.token != '-1') {
                console.log('PUSH: iniciando UPDATE');
                // Se crea el objeto con el token de autenticación y de push
                var userObject = {};
                userObject.token = auth.token;
                userObject.push = auth.push;
                $http.post(window.localStorage.getItem('API_URL') + wsUrl, wrapDataToWsExpectedScheme(userObject), {
                    headers : {
                        'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
                    },
                    transformRequest: transformRequestAsFormPost
                })
                    .then(function (res) {
                        console.log(res);
                    }, function (err) {
                        console.log(err);
                    });
            } else {
                console.log('PUSH: usuario no auth o no push');
            }
        },

        auth: auth,
        countries: countries,
        pedido: pedido
    };
})

.factory('Business', function($http, apiConstants, transformRequestAsFormPost, wrapDataToWsExpectedScheme, GlobalsService) {
    var auth = {
        token: '-1'
    };

    var apiVersion = 'v050100';

    return {
        getCiudades: function () {
            // Se define el nivel de API que hay que atacar
            var wsUrl = apiConstants.wsGetCiudades;
            if (wsUrl.indexOf('?') != -1) {
                wsUrl = wsUrl.replace('?', apiVersion);
            }

            return $http.get(window.localStorage.getItem('API_URL') + wsUrl);
        },

        getListaNegocios: function (searchObject) {
            // Se define el nivel de API que hay que atacar
            var wsUrl = apiConstants.wsGetListaNegocios;
            if (wsUrl.indexOf('?') != -1) {
                wsUrl = wsUrl.replace('?', apiVersion);
            }

            // Se le añade el token de autenticación (en principio esta llamada debería ser pública siempre)
            searchObject.token = auth.token;
            return $http.post(window.localStorage.getItem('API_URL') + wsUrl, wrapDataToWsExpectedScheme(searchObject), {
                headers : {
                    'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
                },
                transformRequest: transformRequestAsFormPost
            });
        },

        getDatosRestaurante: function (businessObject) {
            // Se define el nivel de API que hay que atacar
            var wsUrl = apiConstants.wsGetDatosRestaurante;
            if (wsUrl.indexOf('?') != -1) {
                wsUrl = wsUrl.replace('?', apiVersion);
            }

            // Se le añade el token de autenticación (en principio esta llamada debería ser pública siempre)
            businessObject.token = auth.token;
            return $http.post(window.localStorage.getItem('API_URL') + wsUrl, wrapDataToWsExpectedScheme(businessObject), {
                headers : {
                    'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
                },
                transformRequest: transformRequestAsFormPost
            });
        },

        getContacto: function () {
            // Se define el nivel de API que hay que atacar
            var wsUrl = apiConstants.wsGetContacto;
            if (wsUrl.indexOf('?') != -1) {
                wsUrl = wsUrl.replace('?', apiVersion);
            }

            // Se le añade el token de autenticación (en principio esta llamada debería ser pública siempre)
            var contactObj = {};
            contactObj.token = auth.token;

            // Se le añade el país
            var globalDoc = GlobalsService.getGlobalDoc();
            var country = 'es';
            if (globalDoc.selectedCountry && globalDoc.selectedCountry.code) {
                country = globalDoc.selectedCountry.code;
                //se actualiza la url de los ws
                window.localStorage.setItem('API_URL', globalDoc.selectedCountry.protocol + '://' + globalDoc.selectedCountry.url);
            }else{
                window.localStorage.setItem('API_URL', 'http://'+apiConstants.apiUrl);
            }
            contactObj.country = country;

            return $http.post(window.localStorage.getItem('API_URL') + wsUrl, wrapDataToWsExpectedScheme(contactObj), {
                headers : {
                    'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
                },
                transformRequest: transformRequestAsFormPost
            });
        },

        getVersion: function () {
            // Se le añade el token de autenticación (en principio esta llamada debería ser pública siempre)
            var versionObj = {};
            versionObj.token = auth.token;

            // Se le añade la plataforma y versión
            var platform = 'ios';
            var version = 'v' + apiConstants.appVersionbrowser;
            try {
                platform = device.platform.toLowerCase();

                if (platform === 'ios') {
                    version = 'v' + apiConstants.appVersionios;
                } else if (platform === 'android') {
                    version = 'v' + apiConstants.appVersionandroid;
                }
            } catch (e) {}
            versionObj.platform = platform;
            versionObj.version = version;
            var url = window.localStorage.getItem('API_URL');
            if (url == null
                || url == undefined
                || isNaN(url)){
                url = apiConstants.apiUrl;
            }

            return $http.post('http://'+ url + apiConstants.wsGetVersion, wrapDataToWsExpectedScheme(versionObj), {
                headers : {
                    'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
                },
                transformRequest: transformRequestAsFormPost
            });
        },

        setAuthToken: function (token) {
            auth.token = token;
        },

        setMahalos: function () {
            // Se define el nivel de API que hay que atacar
            var wsUrl = apiConstants.wsSetMahalos;
            if (wsUrl.indexOf('?') != -1) {
                wsUrl = wsUrl.replace('?', apiVersion);
            }

            // Se le añade el token de autenticación
            var mahalosObj = {};
            mahalosObj.token = auth.token;
            return $http.post(window.localStorage.getItem('API_URL') + wsUrl, wrapDataToWsExpectedScheme(mahalosObj), {
                headers : {
                    'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
                },
                transformRequest: transformRequestAsFormPost
            });
        },

        getDescuentos: function () {
            // Se define el nivel de API que hay que atacar
            var wsUrl = apiConstants.wsGetDescuentos;
            if (wsUrl.indexOf('?') != -1) {
                wsUrl = wsUrl.replace('?', apiVersion);
            }

            // Se le añade el token de autenticación
            var descuentosObj = {};
            descuentosObj.token = auth.token;
            return $http.post(window.localStorage.getItem('API_URL') + wsUrl, wrapDataToWsExpectedScheme(descuentosObj), {
                headers : {
                    'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
                },
                transformRequest: transformRequestAsFormPost
            });
        },

        getPedido: function (pedidoObj) {
            // Se define el nivel de API que hay que atacar
            var wsUrl = apiConstants.wsGetPedido;
            if (wsUrl.indexOf('?') != -1) {
                wsUrl = wsUrl.replace('?', apiVersion);
            }

            // Se le añade el token de autenticación
            pedidoObj.token = auth.token;
            return $http.post(window.localStorage.getItem('API_URL') + wsUrl, wrapDataToWsExpectedScheme(pedidoObj), {
                headers : {
                    'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
                },
                transformRequest: transformRequestAsFormPost
            });
        },

        getTransportCost: function (ctrans, cp) {
            // Parámetro por defecto a 0, por si no se le pasa en la función
            cp = cp || 0;
            var transportCost = 0;
            // Primero se comprueba que ctrans no sea un número, hay algun caso que pasa un número
            if (typeof ctrans === 'number') {
                ctrans = ctrans.toString();
            }
            // Se comprueba si es un coste de transporte variable o no
            if (ctrans.indexOf(',') == -1) {
                // Si no lo tiene se devuelve ese mismo valor
                if (ctrans == "") { // resulta que algún restaurante lo manda así...
                    ctrans = "0";
                }
                return ctrans;
            } else {
                var ctransArray = ctrans.split(',');
                // Si tiene un coste variable hay que ver si se envía un cp para extraer su coste concreto o no
                if (cp == 0) {
                    // Si no se envía CP se devuelve el coste más caro
                    for (var i = 0; i < ctransArray.length; i++) {
                        // Cada coste multiple individual viene el la forma '33201:1', se parte a su vez por el ':'
                        var ctransUnitArray = ctransArray[i].split(':');
                        if (parseFloat(ctransUnitArray[1]) > transportCost) {
                            transportCost = parseFloat(ctransUnitArray[1]);
                        }
                    }
                    return transportCost.toString();
                } else {
                    // Si se envía CP se devuelve su coste asociado, en caso de no encontrarlo se devuelve '1'
                    for (var i = 0; i < ctransArray.length; i++) {
                        // Cada coste multiple individual viene el la forma '33201:1', se parte a su vez por el ':'
                        var ctransUnitArray = ctransArray[i].split(':');
                        // Se comprueba si el cp enviado coincide con el cp asociado a ese coste
                        if (ctransUnitArray[0] == cp) {
                            return ctransUnitArray[1];
                        }
                    }
                    return "1";
                }
            }
        },

        setApiVersion: function (wsVersion) {
            apiVersion = wsVersion;
        },

        getApiVersion: function () {
            return apiVersion;
        },

        getUrl: function(){
            return window.localStorage.getItem('API_URL');
        }
    }
});