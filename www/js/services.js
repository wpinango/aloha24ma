angular.module('app.services', [])

.factory('transformRequestAsFormPost', function() {
    function transformRequest (obj) {
        var str = [];
        for(var p in obj)
            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
        return str.join("&");
    }

    return( transformRequest );
})

.factory('wrapDataToWsExpectedScheme', function() {
    function wrapDataIntoDatosVariable (data) {
        var wrapper = {};
        wrapper.datos = JSON.stringify(data);

        return wrapper;
    }

    return( wrapDataIntoDatosVariable );
})

.factory('makeIdReferencia', function () {
    function makeidReferencia(){
        var fecha_completa = new Date();
        var anio = fecha_completa.getFullYear();
        var mes = fecha_completa.getMonth() + 1;
        if (mes < 10)
            mes = '0' + mes;
        var dia = fecha_completa.getDate();
        if (dia < 10)
            dia = '0' + dia;
        var hora = fecha_completa.getHours();
        if (hora < 10)
            hora = '0' + hora;
        var minutos = fecha_completa.getMinutes();
        if (minutos < 10)
            minutos = '0' + minutos;
        var segundos = fecha_completa.getSeconds();
        if (segundos < 10)
            segundos = '0' + segundos;
        anio = (anio.toString()).substring(2,4);
        return anio+''+mes+''+dia+''+hora+''+minutos+''+segundos;
    }

    return ( makeidReferencia() );
})

.factory('getAddressComponentsInfo', function() {
    function getAddressComponentsInfo (addressComponents) {
        console.log('inside getAddressComponentsInfo');
        var componentForm = {
            //street_number: 'short_name',
            //locality: 'long_name',
            postal_code: 'short_name'
        };
        var mapping = {
            //street_number: 'street_number',
            //locality: 'city',
            postal_code: 'postal_code'
        };

        var wrapper = {
            //street_number: '',
            postal_code: '',
            //city: ''
        };

        // Get each component of the address from the place location
        // and fill the corresponding field on the form.
        for (var i = 0; i < addressComponents.length; i++) {
            var addressType = addressComponents[i].types[0];
            if (componentForm[addressType]) {
                var val = addressComponents[i][componentForm[addressType]];
                wrapper[mapping[addressType]] = val;
            }
        }

        return wrapper;
    }

    return( getAddressComponentsInfo );
})

.factory('PaypalService', ['$q', '$ionicPlatform', 'shopSettings', '$filter', '$timeout',
    function ($q, $ionicPlatform, shopSettings, $filter, $timeout) {

    var init_defer;
    /**
     * Service object
     * @type object
     */
    var service = {
        initPaymentUI: initPaymentUI,
        createPayment: createPayment,
        configuration: configuration,
        onPayPalMobileInit: onPayPalMobileInit,
        makePayment: makePayment
    };


    /**
     * @ngdoc method
     * @name initPaymentUI
     * @methodOf app.PaypalService
     * @description
     * Inits the payapl ui with certain envs.
     *
     *
     * @returns {object} Promise paypal ui init done
     */
    function initPaymentUI() {

        init_defer = $q.defer();
        $ionicPlatform.ready().then(function () {

            var clientIDs = {
                "PayPalEnvironmentProduction": shopSettings.payPalProductionId,
                "PayPalEnvironmentSandbox": shopSettings.payPalSandboxId
            };
            PayPalMobile.init(clientIDs, onPayPalMobileInit);
        });

        return init_defer.promise;
    }


    /**
     * @ngdoc method
     * @name createPayment
     * @methodOf app.PaypalService
     * @param {string|number} total total sum. Pattern 12.23
     * @param {string} name name of the item in paypal
     * @description
     * Creates a paypal payment object
     *
     *
     * @returns {object} PayPalPaymentObject
     */
    function createPayment(total, name, currency) {

        // "Sale  == >  immediate payment
        // "Auth" for payment authorization only, to be captured separately at a later time.
        // "Order" for taking an order, with authorization and capture to be done separately at a later time.
        var payment = new PayPalPayment("" + total, currency, "" + name, "Sale");
        return payment;
    }
    /**
     * @ngdoc method
     * @name configuration
     * @methodOf app.PaypalService
     * @description
     * Helper to create a paypal configuration object
     *
     *
     * @returns {object} PayPal configuration
     */
    function configuration() {
        // for more options see `paypal-mobile-js-helper.js`
        var config = new PayPalConfiguration({merchantName: shopSettings.payPalShopName, merchantPrivacyPolicyURL: shopSettings.payPalMerchantPrivacyPolicyURL, merchantUserAgreementURL: shopSettings.payPalMerchantUserAgreementURL});
        return config;
    }

    function onPayPalMobileInit() {
        $ionicPlatform.ready().then(function () {
            // must be called
            // use PayPalEnvironmentNoNetwork mode to get look and feel of the flow
            PayPalMobile.prepareToRender(shopSettings.payPalEnv, configuration(), function () {

                $timeout(function () {
                    init_defer.resolve();
                });

            });
        });
    }

    /**
     * @ngdoc method
     * @name makePayment
     * @methodOf app.PaypalService
     * @param {string|number} total total sum. Pattern 12.23
     * @param {string} name name of the item in paypal
     * @description
     * Performs a paypal single payment
     *
     *
     * @returns {object} Promise gets resolved on successful payment, rejected on error
     */
    function makePayment(total, name, country, currency) {
        var defer = $q.defer();
        //total = $filter('number')(total, 2);
        console.log('var: ' + JSON.stringify(country));
        console.log('var2 ' + currency);
        $ionicPlatform.ready().then(function () {
            PayPalMobile.renderSinglePaymentUI(createPayment(total, name, currency), function (result) {
                $timeout(function () {
                    defer.resolve(result);
                });
            }, function (error) {
                $timeout(function () {
                    defer.reject(error);
                });
            });
        });

        return defer.promise;
    }

    return service;
}])

.factory('AppMinimumVersionCheck', function(apiConstants, $cordovaDialogs, $rootScope, $ionicPlatform) {
    var appVersion = apiConstants.appVersionbrowser;
    var serverAppVersion = apiConstants.appVersionbrowser;
    var APP_ID = {
        "android": "es.aloha.MainAloha",
        "ios": "id1023669329",
        "browser": "id1023669329"
    };
    var platform = 'browser';
    var appId = null;
    var globalDoc = null;

    $ionicPlatform.ready(function() {
        try {
            platform = device.platform.toLowerCase();
        } catch (e) {}
        appId = APP_ID[platform];

        appVersion = apiConstants['appVersion'+platform];
        serverAppVersion = apiConstants['appVersion'+platform];
        //window.localStorage.removeItem('appVersion');
        var lastAppVersion = window.localStorage.getItem('appVersion');
        console.log("----------------------------------------\nlastAppVersion="+lastAppVersion);
        console.log("----------------------------------------\nappVersion="+appVersion);
        if (lastAppVersion == null
            || lastAppVersion != appVersion){
            window.localStorage.setItem('appVersion', appVersion);
            window.localStorage.removeItem('API_URL');
            window.localStorage.removeItem('aloha24DB');
            //GlobalsService.initDB();    
        }
    });

    $rootScope.$on('dbLoaded', function (event, _globalDoc) {
        globalDoc = _globalDoc;
    });

    function checkAPIVersion () {
        $ionicPlatform.ready(function() {
            if (globalDoc && globalDoc.appVersion) {
                // Se limpia la versión actual de la API en el servidor, por si lleva una 'v'
                if (globalDoc.appVersion.substring(0, 1) == 'v') {
                    serverAppVersion = globalDoc.appVersion.substring(1);
                } else {
                    serverAppVersion = globalDoc.appVersion;
                }

                // Se comprueba que la versión de la API que usa la app es la última
                if (appVersion < serverAppVersion) {
                    $cordovaDialogs.alert('Esta versión es obsoleta, actualiza la app para disfrutar de las nuevas mejoras',
                        'Nueva versión disponible', 'Actualizar')
                        .then(function() {
                            window.localStorage.removeItem('API_URL');
                            window.localStorage.removeItem('aloha24DB');
                            window.localStorage.removeItem('appVersion');
                            cordova.plugins.market.open(appId);
                        });
                }
            }
        });
    }

    return ( checkAPIVersion );
})
