# CHANGELOG
- - -
### v2.3.0 (10 Jul, 2017)
* Selección inicial de país y cambio de moneda, contacto y restricción en búsquedas de autocomplete a ese territorio
* Push & Analytics Firebase
* Comprobación versión y forzado a actualizar, nueva forma
* Uso de versión de API dinámica tras petición al servidor
* Corrección de bugs
### v2.2.1 (03 Abr, 2017)
* Corregido error apertura WhatsApp en iOS
### v2.2.0 (28 Mar, 2017)
* Mejoras de usabilidad, pedidos más rápidos: reutilizar pedido, uso de direcciones habituales
* Mejora y optimización de reparto
* Categoría negocios locales
* Corrección de errores
* (Interno) Cambio a uso de ws con coordenadas geográficas. Nueva lógica de cálculo de transporte variable en función de la geolocalización. Cambios en vista principal, dos categorías para elegir qué buscar, búsqueda rápida por direcciones habituales, reordenación elementos. Notificaciones PUSH. Cambios en vista favoritos. Reutilización de pedidos. Cambios en cabeceras restaurantes. Cambio de ws en comprobación de versión mínima de app. Añadido descuento por porcentaje
### v2.1.0 (17 Ene, 2017)
* Direcciones geolocalizadas para mejora en gestión y reparto
* Corrección de bugs
* (Interno) Forzar actualización de App en stores
* (Interno) Moneda configurable
### v2.0.2 (17 Oct, 2016)
* Corregido error de apertura de WhatsApp en iOS 10
### v2.0.1 (07 Oct, 2016)
* Recuperar contraseña
### v2.0.0 (24 Sep, 2016)
* Versión inicial del rediseño hecho por Gomeru Apps

